/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createStudent = /* GraphQL */ `
  mutation CreateStudent(
    $input: CreateStudentInput!
    $condition: ModelStudentConditionInput
  ) {
    createStudent(input: $input, condition: $condition) {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        nextToken
      }
      requests {
        nextToken
      }
      availabilities {
        nextToken
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        nextToken
      }
      updatedInformation
      conversations {
        nextToken
      }
      messages {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateStudent = /* GraphQL */ `
  mutation UpdateStudent(
    $input: UpdateStudentInput!
    $condition: ModelStudentConditionInput
  ) {
    updateStudent(input: $input, condition: $condition) {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        nextToken
      }
      requests {
        nextToken
      }
      availabilities {
        items {
          day
          startTime
          endTime
        }
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        items {
          subject {
            id
            code
            name
          }
        }
      }
      updatedInformation
      conversations {
        nextToken
      }
      messages {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteStudent = /* GraphQL */ `
  mutation DeleteStudent(
    $input: DeleteStudentInput!
    $condition: ModelStudentConditionInput
  ) {
    deleteStudent(input: $input, condition: $condition) {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        nextToken
      }
      requests {
        nextToken
      }
      availabilities {
        nextToken
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        nextToken
      }
      updatedInformation
      conversations {
        nextToken
      }
      messages {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createConversationStudentJoin = /* GraphQL */ `
  mutation CreateConversationStudentJoin(
    $input: CreateConversationStudentJoinInput!
    $condition: ModelConversationStudentJoinConditionInput
  ) {
    createConversationStudentJoin(input: $input, condition: $condition) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateConversationStudentJoin = /* GraphQL */ `
  mutation UpdateConversationStudentJoin(
    $input: UpdateConversationStudentJoinInput!
    $condition: ModelConversationStudentJoinConditionInput
  ) {
    updateConversationStudentJoin(input: $input, condition: $condition) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteConversationStudentJoin = /* GraphQL */ `
  mutation DeleteConversationStudentJoin(
    $input: DeleteConversationStudentJoinInput!
    $condition: ModelConversationStudentJoinConditionInput
  ) {
    deleteConversationStudentJoin(input: $input, condition: $condition) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createConversation = /* GraphQL */ `
  mutation CreateConversation(
    $input: CreateConversationInput!
    $condition: ModelConversationConditionInput
  ) {
    createConversation(input: $input, condition: $condition) {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const updateConversation = /* GraphQL */ `
  mutation UpdateConversation(
    $input: UpdateConversationInput!
    $condition: ModelConversationConditionInput
  ) {
    updateConversation(input: $input, condition: $condition) {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const deleteConversation = /* GraphQL */ `
  mutation DeleteConversation(
    $input: DeleteConversationInput!
    $condition: ModelConversationConditionInput
  ) {
    deleteConversation(input: $input, condition: $condition) {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const createMessage = /* GraphQL */ `
  mutation CreateMessage(
    $input: CreateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    createMessage(input: $input, condition: $condition) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const updateMessage = /* GraphQL */ `
  mutation UpdateMessage(
    $input: UpdateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    updateMessage(input: $input, condition: $condition) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const deleteMessage = /* GraphQL */ `
  mutation DeleteMessage(
    $input: DeleteMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    deleteMessage(input: $input, condition: $condition) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const createJoinGroupRequests = /* GraphQL */ `
  mutation CreateJoinGroupRequests(
    $input: CreateJoinGroupRequestsInput!
    $condition: ModelJoinGroupRequestsConditionInput
  ) {
    createJoinGroupRequests(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateJoinGroupRequests = /* GraphQL */ `
  mutation UpdateJoinGroupRequests(
    $input: UpdateJoinGroupRequestsInput!
    $condition: ModelJoinGroupRequestsConditionInput
  ) {
    updateJoinGroupRequests(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteJoinGroupRequests = /* GraphQL */ `
  mutation DeleteJoinGroupRequests(
    $input: DeleteJoinGroupRequestsInput!
    $condition: ModelJoinGroupRequestsConditionInput
  ) {
    deleteJoinGroupRequests(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createGroup = /* GraphQL */ `
  mutation CreateGroup(
    $input: CreateGroupInput!
    $condition: ModelGroupConditionInput
  ) {
    createGroup(input: $input, condition: $condition) {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateGroup = /* GraphQL */ `
  mutation UpdateGroup(
    $input: UpdateGroupInput!
    $condition: ModelGroupConditionInput
  ) {
    updateGroup(input: $input, condition: $condition) {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteGroup = /* GraphQL */ `
  mutation DeleteGroup(
    $input: DeleteGroupInput!
    $condition: ModelGroupConditionInput
  ) {
    deleteGroup(input: $input, condition: $condition) {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createGroupStudent = /* GraphQL */ `
  mutation CreateGroupStudent(
    $input: CreateGroupStudentInput!
    $condition: ModelGroupStudentConditionInput
  ) {
    createGroupStudent(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateGroupStudent = /* GraphQL */ `
  mutation UpdateGroupStudent(
    $input: UpdateGroupStudentInput!
    $condition: ModelGroupStudentConditionInput
  ) {
    updateGroupStudent(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteGroupStudent = /* GraphQL */ `
  mutation DeleteGroupStudent(
    $input: DeleteGroupStudentInput!
    $condition: ModelGroupStudentConditionInput
  ) {
    deleteGroupStudent(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createGroupSubject = /* GraphQL */ `
  mutation CreateGroupSubject(
    $input: CreateGroupSubjectInput!
    $condition: ModelGroupSubjectConditionInput
  ) {
    createGroupSubject(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateGroupSubject = /* GraphQL */ `
  mutation UpdateGroupSubject(
    $input: UpdateGroupSubjectInput!
    $condition: ModelGroupSubjectConditionInput
  ) {
    updateGroupSubject(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteGroupSubject = /* GraphQL */ `
  mutation DeleteGroupSubject(
    $input: DeleteGroupSubjectInput!
    $condition: ModelGroupSubjectConditionInput
  ) {
    deleteGroupSubject(input: $input, condition: $condition) {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createStudentSubject = /* GraphQL */ `
  mutation CreateStudentSubject(
    $input: CreateStudentSubjectInput!
    $condition: ModelStudentSubjectConditionInput
  ) {
    createStudentSubject(input: $input, condition: $condition) {
      id
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateStudentSubject = /* GraphQL */ `
  mutation UpdateStudentSubject(
    $input: UpdateStudentSubjectInput!
    $condition: ModelStudentSubjectConditionInput
  ) {
    updateStudentSubject(input: $input, condition: $condition) {
      id
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteStudentSubject = /* GraphQL */ `
  mutation DeleteStudentSubject(
    $input: DeleteStudentSubjectInput!
    $condition: ModelStudentSubjectConditionInput
  ) {
    deleteStudentSubject(input: $input, condition: $condition) {
      id
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createUniversity = /* GraphQL */ `
  mutation CreateUniversity(
    $input: CreateUniversityInput!
    $condition: ModelUniversityConditionInput
  ) {
    createUniversity(input: $input, condition: $condition) {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUniversity = /* GraphQL */ `
  mutation UpdateUniversity(
    $input: UpdateUniversityInput!
    $condition: ModelUniversityConditionInput
  ) {
    updateUniversity(input: $input, condition: $condition) {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUniversity = /* GraphQL */ `
  mutation DeleteUniversity(
    $input: DeleteUniversityInput!
    $condition: ModelUniversityConditionInput
  ) {
    deleteUniversity(input: $input, condition: $condition) {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createSubject = /* GraphQL */ `
  mutation CreateSubject(
    $input: CreateSubjectInput!
    $condition: ModelSubjectConditionInput
  ) {
    createSubject(input: $input, condition: $condition) {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateSubject = /* GraphQL */ `
  mutation UpdateSubject(
    $input: UpdateSubjectInput!
    $condition: ModelSubjectConditionInput
  ) {
    updateSubject(input: $input, condition: $condition) {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteSubject = /* GraphQL */ `
  mutation DeleteSubject(
    $input: DeleteSubjectInput!
    $condition: ModelSubjectConditionInput
  ) {
    deleteSubject(input: $input, condition: $condition) {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createCampus = /* GraphQL */ `
  mutation CreateCampus(
    $input: CreateCampusInput!
    $condition: ModelCampusConditionInput
  ) {
    createCampus(input: $input, condition: $condition) {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateCampus = /* GraphQL */ `
  mutation UpdateCampus(
    $input: UpdateCampusInput!
    $condition: ModelCampusConditionInput
  ) {
    updateCampus(input: $input, condition: $condition) {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteCampus = /* GraphQL */ `
  mutation DeleteCampus(
    $input: DeleteCampusInput!
    $condition: ModelCampusConditionInput
  ) {
    deleteCampus(input: $input, condition: $condition) {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createTimeSlot = /* GraphQL */ `
  mutation CreateTimeSlot(
    $input: CreateTimeSlotInput!
    $condition: ModelTimeSlotConditionInput
  ) {
    createTimeSlot(input: $input, condition: $condition) {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateTimeSlot = /* GraphQL */ `
  mutation UpdateTimeSlot(
    $input: UpdateTimeSlotInput!
    $condition: ModelTimeSlotConditionInput
  ) {
    updateTimeSlot(input: $input, condition: $condition) {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteTimeSlot = /* GraphQL */ `
  mutation DeleteTimeSlot(
    $input: DeleteTimeSlotInput!
    $condition: ModelTimeSlotConditionInput
  ) {
    deleteTimeSlot(input: $input, condition: $condition) {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createAvailability = /* GraphQL */ `
  mutation CreateAvailability(
    $input: CreateAvailabilityInput!
    $condition: ModelAvailabilityConditionInput
  ) {
    createAvailability(input: $input, condition: $condition) {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateAvailability = /* GraphQL */ `
  mutation UpdateAvailability(
    $input: UpdateAvailabilityInput!
    $condition: ModelAvailabilityConditionInput
  ) {
    updateAvailability(input: $input, condition: $condition) {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteAvailability = /* GraphQL */ `
  mutation DeleteAvailability(
    $input: DeleteAvailabilityInput!
    $condition: ModelAvailabilityConditionInput
  ) {
    deleteAvailability(input: $input, condition: $condition) {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
