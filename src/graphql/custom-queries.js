/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getStudentMessages = /* GraphQL */ `
  query GetStudentMessages($id: ID!) {
    getStudent(id: $id) {
      conversations {
        items {
          conversation {
            id
            name
            messages {
              items {
                id
                content
                author {
                  name
                  id
                  avatar
                }
                createdAt
              }
            }
            members {
              items {
                student {
                  id
                  name
                  avatar
                }
              }
            }
          }
        }
      }
    }
  }
`;
export const getStudent = /* GraphQL */ `
  query GetStudent($id: ID!) {
    getStudent(id: $id) {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        # createdAt
        # updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        items {
          id
          group {
            id
            name
            avatar
            description
            students {
              items {
                student {
                  id
                  avatar
                  name
                }
              }
            }
          }
        }
      }
      availabilities {
        items {
          day
          endTime
          startTime
          id
        }
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        items {
          id
          subject {
            name
            code
            id
            students {
              items {
                subject {
                  code
                }
                student {
                  id
                  name
                  university {
                    abbreviation
                  }
                  campus {
                    name
                  }
                }
              }
            }
          }
        }
      }
      updatedInformation
      createdAt
      updatedAt
    }
  }
`;
export const listStudents = /* GraphQL */ `
  query ListStudents(
    $filter: ModelStudentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listStudents(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        university {
          abbreviation
        }
        campus {
          name
        }
        updatedInformation
        createdAt
        updatedInformation
      }
      nextToken
    }
  }
`;
export const getConversation = /* GraphQL */ `
  query GetConversation($id: ID!) {
    getConversation(id: $id) {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const listConversations = /* GraphQL */ `
  query ListConversations(
    $filter: ModelConversationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listConversations(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getMessage = /* GraphQL */ `
  query GetMessage($id: ID!) {
    getMessage(id: $id) {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const listMessages = /* GraphQL */ `
  query ListMessages(
    $filter: ModelMessageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMessages(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getGroup = /* GraphQL */ `
  query GetGroup($id: ID!) {
    getGroup(id: $id) {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listGroups = /* GraphQL */ `
  query ListGroups(
    $filter: ModelGroupFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGroups(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUniversity = /* GraphQL */ `
  query GetUniversity($id: ID!) {
    getUniversity(id: $id) {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUniversitys = /* GraphQL */ `
  query ListUniversitys(
    $filter: ModelUniversityFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUniversitys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSubject = /* GraphQL */ `
  query GetSubject($id: ID!) {
    getSubject(id: $id) {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listSubjects = /* GraphQL */ `
  query ListSubjects(
    $filter: ModelSubjectFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSubjects(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        code
        name
      }
      nextToken
    }
  }
`;
export const getCampus = /* GraphQL */ `
  query GetCampus($id: ID!) {
    getCampus(id: $id) {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listCampuss = /* GraphQL */ `
  query ListCampuss(
    $filter: ModelCampusFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCampuss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTimeSlot = /* GraphQL */ `
  query GetTimeSlot($id: ID!) {
    getTimeSlot(id: $id) {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listTimeSlots = /* GraphQL */ `
  query ListTimeSlots(
    $filter: ModelTimeSlotFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTimeSlots(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        day
        startTime
        endTime
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getAvailability = /* GraphQL */ `
  query GetAvailability($id: ID!) {
    getAvailability(id: $id) {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listAvailabilitys = /* GraphQL */ `
  query ListAvailabilitys(
    $filter: ModelAvailabilityFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAvailabilitys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        day
        startTime
        endTime
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const getStudentSubjectGroups = /* GraphQL */ `
  query getStudentSubjectGroups($id: ID!) {
    getStudent(id: $id) {
      subjects {
        items {
          subject {
            code
            groups {
              items {
                group {
                  id
                  name
                  joinable
                  avatar
                  students {
                    items {
                      student {
                        id
                        name
                        avatar
                      }
                    }
                  }
                  schedule {
                    items {
                      id
                      day
                      description
                      color
                      startTime
                      endTime
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
