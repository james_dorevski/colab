/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateStudent = /* GraphQL */ `
  subscription OnCreateStudent {
    onCreateStudent {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        nextToken
      }
      requests {
        nextToken
      }
      availabilities {
        nextToken
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        nextToken
      }
      updatedInformation
      conversations {
        nextToken
      }
      messages {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStudent = /* GraphQL */ `
  subscription OnUpdateStudent {
    onUpdateStudent {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        nextToken
      }
      requests {
        nextToken
      }
      availabilities {
        nextToken
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        nextToken
      }
      updatedInformation
      conversations {
        nextToken
      }
      messages {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStudent = /* GraphQL */ `
  subscription OnDeleteStudent {
    onDeleteStudent {
      id
      name
      email
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      campus {
        id
        name
        createdAt
        updatedAt
      }
      groups {
        nextToken
      }
      requests {
        nextToken
      }
      availabilities {
        nextToken
      }
      studentNumber
      studentType
      courseType
      avatar
      languageSpoken
      preferredLanguage
      wam
      course
      subjects {
        nextToken
      }
      updatedInformation
      conversations {
        nextToken
      }
      messages {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateConversationStudentJoin = /* GraphQL */ `
  subscription OnCreateConversationStudentJoin {
    onCreateConversationStudentJoin {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateConversationStudentJoin = /* GraphQL */ `
  subscription OnUpdateConversationStudentJoin {
    onUpdateConversationStudentJoin {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteConversationStudentJoin = /* GraphQL */ `
  subscription OnDeleteConversationStudentJoin {
    onDeleteConversationStudentJoin {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateConversation = /* GraphQL */ `
  subscription OnCreateConversation {
    onCreateConversation {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateConversation = /* GraphQL */ `
  subscription OnUpdateConversation {
    onUpdateConversation {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteConversation = /* GraphQL */ `
  subscription OnDeleteConversation {
    onDeleteConversation {
      id
      members {
        nextToken
      }
      messages {
        nextToken
      }
      name
      createdAt
      updatedAt
    }
  }
`;
export const onCreateMessage = /* GraphQL */ `
  subscription OnCreateMessage {
    onCreateMessage {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMessage = /* GraphQL */ `
  subscription OnUpdateMessage {
    onUpdateMessage {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMessage = /* GraphQL */ `
  subscription OnDeleteMessage {
    onDeleteMessage {
      id
      conversation {
        id
        name
        createdAt
        updatedAt
      }
      author {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      content
      createdAt
      updatedAt
    }
  }
`;
export const onCreateJoinGroupRequests = /* GraphQL */ `
  subscription OnCreateJoinGroupRequests {
    onCreateJoinGroupRequests {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateJoinGroupRequests = /* GraphQL */ `
  subscription OnUpdateJoinGroupRequests {
    onUpdateJoinGroupRequests {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteJoinGroupRequests = /* GraphQL */ `
  subscription OnDeleteJoinGroupRequests {
    onDeleteJoinGroupRequests {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateGroup = /* GraphQL */ `
  subscription OnCreateGroup {
    onCreateGroup {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGroup = /* GraphQL */ `
  subscription OnUpdateGroup {
    onUpdateGroup {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGroup = /* GraphQL */ `
  subscription OnDeleteGroup {
    onDeleteGroup {
      id
      name
      subjects {
        nextToken
      }
      schedule {
        nextToken
      }
      students {
        nextToken
      }
      joinable
      requests {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateGroupStudent = /* GraphQL */ `
  subscription OnCreateGroupStudent {
    onCreateGroupStudent {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGroupStudent = /* GraphQL */ `
  subscription OnUpdateGroupStudent {
    onUpdateGroupStudent {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGroupStudent = /* GraphQL */ `
  subscription OnDeleteGroupStudent {
    onDeleteGroupStudent {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateGroupSubject = /* GraphQL */ `
  subscription OnCreateGroupSubject {
    onCreateGroupSubject {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGroupSubject = /* GraphQL */ `
  subscription OnUpdateGroupSubject {
    onUpdateGroupSubject {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGroupSubject = /* GraphQL */ `
  subscription OnDeleteGroupSubject {
    onDeleteGroupSubject {
      id
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateStudentSubject = /* GraphQL */ `
  subscription OnCreateStudentSubject {
    onCreateStudentSubject {
      id
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStudentSubject = /* GraphQL */ `
  subscription OnUpdateStudentSubject {
    onUpdateStudentSubject {
      id
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStudentSubject = /* GraphQL */ `
  subscription OnDeleteStudentSubject {
    onDeleteStudentSubject {
      id
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      subject {
        id
        code
        name
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUniversity = /* GraphQL */ `
  subscription OnCreateUniversity {
    onCreateUniversity {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUniversity = /* GraphQL */ `
  subscription OnUpdateUniversity {
    onUpdateUniversity {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUniversity = /* GraphQL */ `
  subscription OnDeleteUniversity {
    onDeleteUniversity {
      id
      name
      abbreviation
      emailDomains
      courses
      sessionDuration
      subjects {
        nextToken
      }
      students {
        nextToken
      }
      campuses {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSubject = /* GraphQL */ `
  subscription OnCreateSubject {
    onCreateSubject {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSubject = /* GraphQL */ `
  subscription OnUpdateSubject {
    onUpdateSubject {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSubject = /* GraphQL */ `
  subscription OnDeleteSubject {
    onDeleteSubject {
      id
      code
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      students {
        nextToken
      }
      groups {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCampus = /* GraphQL */ `
  subscription OnCreateCampus {
    onCreateCampus {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCampus = /* GraphQL */ `
  subscription OnUpdateCampus {
    onUpdateCampus {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCampus = /* GraphQL */ `
  subscription OnDeleteCampus {
    onDeleteCampus {
      id
      name
      university {
        id
        name
        abbreviation
        emailDomains
        courses
        sessionDuration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTimeSlot = /* GraphQL */ `
  subscription OnCreateTimeSlot {
    onCreateTimeSlot {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTimeSlot = /* GraphQL */ `
  subscription OnUpdateTimeSlot {
    onUpdateTimeSlot {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTimeSlot = /* GraphQL */ `
  subscription OnDeleteTimeSlot {
    onDeleteTimeSlot {
      id
      day
      startTime
      endTime
      group {
        id
        name
        joinable
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateAvailability = /* GraphQL */ `
  subscription OnCreateAvailability {
    onCreateAvailability {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateAvailability = /* GraphQL */ `
  subscription OnUpdateAvailability {
    onUpdateAvailability {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteAvailability = /* GraphQL */ `
  subscription OnDeleteAvailability {
    onDeleteAvailability {
      id
      day
      startTime
      endTime
      student {
        id
        name
        email
        studentNumber
        studentType
        courseType
        avatar
        languageSpoken
        preferredLanguage
        wam
        course
        updatedInformation
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
