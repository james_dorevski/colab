import React from "react";
import { Provider as PaperProvider } from "react-native-paper";
import { AppLoading } from "expo";
import { useFonts } from "expo-font";
import { StatusBar } from "expo-status-bar";
import Amplify from "aws-amplify";
import { Authenticator } from "aws-amplify-react-native";
import awsconfig from "./aws-exports";
import InternalApp from "./mui/InternalApp";
import SignUpScreen from "./mui/screens/authentication/SignUpScreen";
import SignInScreen from "./mui/screens/authentication/SignInScreen";
import ConfirmSignUpScreen from "./mui/screens/authentication/ConfirmSignUpScreen";
import ForgotPasswordScreen from "./mui/screens/authentication/ForgotPasswordScreen";
import RequireNewPasswordScreen from "./mui/screens/authentication/RequireNewPasswordScreen";
import { LogBox } from "react-native";

Amplify.configure({
  ...awsconfig,
  Analytics: {
    disabled: true,
  },
});

export default function App() {
  LogBox.ignoreAllLogs(true);

  let [fontsLoaded] = useFonts({
    raleway: require("./assets/fonts/Raleway-Regular.ttf"),
    "raleway-medium": require("./assets/fonts/Raleway-Medium.ttf"),
    "raleway-bold": require("./assets/fonts/Raleway-Bold.ttf"),
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <>
        <PaperProvider>
          <Authenticator
            authState="signIn"
            usernameAttributes="email"
            hideDefault={true}
            theme={{ container: { flex: 1 } }}
          >
            <InternalApp />
            <SignUpScreen override={"SignUp"} />
            <SignInScreen override={"SignIn"} />
            <ConfirmSignUpScreen override={"ConfirmSignUp"} />
            <ForgotPasswordScreen override={"ForgotPassword"} />
            <RequireNewPasswordScreen override={"requireNewPassword"} />
          </Authenticator>
          <StatusBar style="auto" />
        </PaperProvider>
      </>
    );
  }
}
