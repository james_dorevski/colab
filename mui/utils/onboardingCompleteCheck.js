import AsyncStorage from "@react-native-community/async-storage"

const ONBOARDING_COMPLETE = 'onboardingComplete';

export function setOnboardingComplete() {
    AsyncStorage.setItem(ONBOARDING_COMPLETE, 'true');
}

export default async function onboardingCompleteCheck() {
    try {
        const onboardingComplete = await AsyncStorage.getItem(ONBOARDING_COMPLETE);
        if (onboardingComplete === null) {
            setOnboardingComplete();
            return true;
        }
        return false;
    } catch (error) {
        return false;
    }
}