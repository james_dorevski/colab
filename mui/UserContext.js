import React, { useState } from "react";

export const UserContext = React.createContext();

// eslint-disable-next-line react/prop-types
export const UserProvider = ({ children }) => {
  const [id, setId] = useState();
  const [name, setName] = useState();
  const [avatar, setAvatar] = useState();
  const [campus, setCampus] = useState();
  const [university, setUniversity] = useState("");
  const [wam, setWam] = useState();
  const [preferredLanguage, setPreferredLanguage] = useState("English");
  const [groups, setGroups] = useState([]);
  const [messages, setMessages] = useState([]);
  const [subjects, setSubjects] = useState([]);
  const [availability, setAvailability] = useState();
  const [suggestedGroups, setSuggestedGroups] = useState();
  const [existingSubjectIds, setExistingSubjectIds] = useState([]);
  const [subjectCodes, setSubjectCodes] = useState();

  const user = {
    id,
    name,
    avatar,
    campus,
    university,
    wam,
    preferredLanguage,
    groups,
    subjects,
    availability,
    messages,
    suggestedGroups,
    existingSubjectIds,
    subjectCodes,
    setId,
    setName,
    setAvatar,
    setCampus,
    setUniversity,
    setWam,
    setPreferredLanguage,
    setGroups,
    setMessages,
    setSubjects,
    setAvailability,
    setSuggestedGroups,
    setExistingSubjectIds,
    setSubjectCodes,
  };

  return <UserContext.Provider value={user}>{children}</UserContext.Provider>;
};

export const useUserContextState = () => {
  const context = React.useContext(UserContext);

  if (context === undefined) {
    throw new Error("useUserContextState must be within a UserProvider");
  }
  return context;
};
