import { StyleSheet, Platform, StatusBar } from "react-native";
import Colors from "../constants/Colors";

export default StyleSheet.create({
  androidSafeArea: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  emptyState: {
    paddingBottom: "5%",
  },
  groupsRendererCard: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    height: 60,
  },
  errorText: {
    color: Colors.errorText,
  },
  searchBarStyles: {
    borderRadius: 15,
    marginTop: "5%",
    marginBottom: "2%",
    marginHorizontal: "5%",
  },
});
