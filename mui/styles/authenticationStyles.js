import { StyleSheet } from "react-native";

export default StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  bigText: {
    fontFamily: "raleway",
    fontSize: 30,
  },
  littleText: {
    fontFamily: "raleway",
    fontSize: 20,
    marginTop: "5%",
  },
  buttonContainer: {
    alignItems: "center",
    marginTop: "5%",
  },
  primaryButton: {
    height: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  secondaryButton: {
    height: 45, 
    justifyContent: "center",
    alignItems: "center", 
    marginRight: "5%"
  }
});
