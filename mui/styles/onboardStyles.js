import { StyleSheet } from "react-native";

export default StyleSheet.create({
  screen: {
    padding: 30,
    backgroundColor: "white",
    flex: 1,
  },
  image: {
    width: 110,
    height: 120,
  },
  bigText: {
    fontFamily: "raleway",
    fontSize: 30,
  },
  littleText: {
    fontFamily: "raleway",
    fontSize: 20,
    marginTop: "5%",
  },
  buttonContainer: {
    flexDirection: "row-reverse",
    alignItems: "center",
    marginTop: "5%",
    position: "absolute",
    bottom: "5%",
    left: "20%",
  },
  primaryButton: {
    height: 45,
    width: 110,
    justifyContent: "center",
    alignItems: "center",
  },
  secondaryButton: {
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    marginRight: "2%",
  },
});
