import React from "react";
import ColabNavigator from "./navigation/ColabNavigator";
import { OnboardingProvider } from "./screens/onboarding/OnboardingContext";

// eslint-disable-next-line no-undef
export default InternalApp = (props) => {
  if (props.authState === "signedIn") {
    return (
      <OnboardingProvider>
        <ColabNavigator />
      </OnboardingProvider>
    );
  } else {
    return null;
  }
};
