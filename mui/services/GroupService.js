import { API, graphqlOperation } from "aws-amplify";
import * as mutations from "../../src/graphql/custom-mutations";
import { Alert } from "react-native";
import { Auth } from "@aws-amplify/auth";

export async function createGroup(values, avatar) {
  try {
    return await API.graphql(
      graphqlOperation(mutations.createGroup, {
        input: {
          name: values.name,
          description: values.description,
          avatar: avatar,
          joinable: true,
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Creating Group", err.message, [{ text: "Ok" }]);
  }
}

export async function addStudentToGroup(groupId) {
  try {
    await API.graphql(
      graphqlOperation(mutations.createGroupStudent, {
        input: {
          groupStudentGroupId: groupId,
          groupStudentStudentId: Auth.user.attributes.sub,
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Adding Student to Group", err.message, [{ text: "Ok" }]);
  }
}

export async function addSubjectToGroup(groupId, subjectId) {
  try {
    await API.graphql(
      graphqlOperation(mutations.createGroupSubject, {
        input: {
          groupSubjectGroupId: groupId,
          groupSubjectSubjectId: subjectId,
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Adding Subject to Group", err.message, [{ text: "Ok" }]);
  }
}
