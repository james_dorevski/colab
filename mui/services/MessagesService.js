import { API, graphqlOperation } from "aws-amplify";
import * as mutations from "../../src/graphql/custom-mutations";
import { Alert } from "react-native";
import * as queries from "../../src/graphql/custom-queries";

// export const navigateToNewOrExisting = (user, item, {navigation}) => {
//     returnConversations(user.id)
//       .then((result) => result.data.getStudent.conversations.items)
//       .then((conversations) =>
//         conversations.map((obj) => {
//           //if the recipient is a member of a conversation with the user, return their student object
//           const students = obj.conversation.members.items.filter(
//             (member) => {
//               return member.student.id === item.id;
//             }
//           );
//
//           if (students !== []) {
//             navigation.navigate("Chat", {
//               thread: item,
//               conversationId: obj.conversation.id,
//               messages: obj.conversation.messages.items,
//             });
//           } else {
//             createConversation(item, user)
//               .then((result) => {
//                 addUserToConversation(
//                   result.data.createConversation.id,
//                   item
//                 ).then(
//                   addUserToConversation(
//                     result.data.createConversation.id,
//                     user
//                   )
//                 );
//                 return result;
//               })
//               .then((result) => {
//                 navigation.navigate("Chat", {
//                   thread: item,
//                   conversationId: result.data.createConversation.id,
//                 });
//               });
//           }
//         })
//       );
//   }

export const createMessage = async (newMessage, user, conversationId) => {
  try {
    await API.graphql(
      graphqlOperation(mutations.createMessage, {
        input: {
          messageConversationId: conversationId,
          messageAuthorId: user.id,
          content: newMessage.text,
        },
        options: {
          fetchPolicy: "cache-and-network",
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Creating Message", err.message, [{ text: "Ok" }]);
  }
};

export const returnConversations = async (userId) => {
  try {
    return await API.graphql(
      graphqlOperation(queries.getStudentMessages, {
        id: userId.toString(),
        options: {
          fetchPolicy: "cache-and-network",
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Retrieving User's Conversations", err.message, [
      {
        text: "OK",
      },
    ]);
  }
};

export async function createConversation(messageRecipient, user) {
  try {
    return await API.graphql(
      graphqlOperation(mutations.createConversation, {
        input: {
          name: `${user.name} and ${messageRecipient.name}`,
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Creating Conversation", err.message, [{ text: "Ok" }]);
  }
}

export async function addUserToConversation(conversationId, user) {
  try {
    await API.graphql(
      graphqlOperation(mutations.createConversationStudentJoin, {
        input: {
          conversationStudentJoinConversationId: conversationId,
          conversationStudentJoinStudentId: user.id,
        },
      })
    );
  } catch (err) {
    Alert.alert("Error Adding User to Conversation", err.message, [
      { text: "Ok" },
    ]);
  }
}
