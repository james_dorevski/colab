import { API, Auth, graphqlOperation } from "aws-amplify";
import * as queries from "../../src/graphql/custom-queries";
import * as mutations from "../../src/graphql/custom-mutations";
import { Alert } from "react-native";

const handleError = (text, errorMessage) => {
  Alert.alert(text, errorMessage, [{ text: "Ok" }]);
};

export const handleSignOut = async () => {
  await Auth.signOut().catch((err) => {
    handleError("Error signing out", err.message);
  });
};

/* queries */
export const getUserInformation = async (id) => {
  const student = await API.graphql(
    graphqlOperation(queries.getStudent, { id: id })
  ).catch((err) => {
    handleError("Error retrieving user profile", err.message);
  });

  const groups = await API.graphql(
    graphqlOperation(queries.getStudentSubjectGroups, {
      id: id,
    })
  );

  return {
    student: student.data.getStudent,
    groups: groups.data.getStudent,
  };
};

/* mutations */
export const updateAvatar = async (id, newAvatar) => {
  await API.graphql(
    graphqlOperation(mutations.updateStudent, {
      input: { id: id, avatar: newAvatar },
    })
  ).catch((err) => {
    handleError("Error updating user avatar", err.message);
  });
};

export const updateCampus = async (id, newCampusId) => {
  const results = await API.graphql(
    graphqlOperation(mutations.updateStudent, {
      input: { id: id, studentCampusId: newCampusId },
    })
  ).catch((err) => {
    handleError("Error updating campus", err.message);
  });
  return results.data.updateStudent.campus.name;
};

export const updatePassword = async (values) => {
  Auth.currentAuthenticatedUser()
    .then((user) => {
      return Auth.changePassword(user, values.oldPassword, values.newPassword);
    })
    .then(() => {
      Alert.alert("Info", "Password changed successfully.", [{ text: "Ok" }]);
    })
    .catch((err) => {
      handleError("Error changing password", err.message);
    });
};

export const updatePreferredLanguage = async (id, newPreferredLanguage) => {
  await API.graphql(
    graphqlOperation(mutations.updateStudent, {
      input: { id: id, preferredLanguage: newPreferredLanguage },
    })
  ).catch((err) => {
    handleError("Error updating preferred language", err.message);
  });
};

export const updateWam = async (id, newWam) => {
  await API.graphql(
    graphqlOperation(mutations.updateStudent, {
      input: { id: id, wam: newWam },
    })
  ).catch((err) => {
    handleError("Error updating wam", err.message);
  });
};
