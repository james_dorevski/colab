import { API, Auth, graphqlOperation } from "aws-amplify";
import * as queries from "../../src/graphql/queries";
import { Alert } from "react-native";

export function getUniqueListBy(arr, key) {
  const newArr = arr.filter((item) => item !== undefined);
  return [...new Map(newArr.map((item) => [item[key], item])).values()];
}

export async function returnUserSubjects() {
  try {
    const results = await API.graphql(
      graphqlOperation(queries.getStudent, { id: Auth.user.attributes.sub })
    );
    return results.data.getStudent.subjects;
  } catch (err) {
    Alert.alert("Error retrieving community", err.message, [{ text: "Ok" }]);
  }
}

export function returnCommunity(subjects, user) {
  return subjects?.items.map((subject) => {
    return subject.subject?.students.items.map((student) => {
      if (student.student?.name !== user.name) {
        return {
          name: student.student.name,
          id: student.student.id,
          avatar: student.student.avatar,
          university: student.student.university.abbreviation,
          campus: student.student.campus.name,
          subject: student.subject.code,
        };
      }
    });
  });
}
