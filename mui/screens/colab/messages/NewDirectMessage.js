import { FlatList, SafeAreaView, StyleSheet, View } from "react-native";
import React, { useState } from "react";
import globalStyles from "../../../styles/globalStyles";
import { Divider, Searchbar } from "react-native-paper";
import {
  getUniqueListBy,
  returnUserSubjects,
  returnCommunity,
} from "../../../services/CommunityService";
import { useUserContextState } from "../../../UserContext";
import Loading from "../../../components/Loading";
import ContactCard from "../../../components/ContactCard";
import {
  addUserToConversation,
  createConversation,
  returnConversations,
} from "../../../services/MessagesService";

const NewDirectMessage = ({ navigation }) => {
  const user = useUserContextState();

  const [community, setCommunity] = useState([]);
  const [filteredFriends, setFilteredFriends] = useState([]);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  React.useEffect(() => {
    setLoading(true);
    returnUserSubjects(user)
      .then((subjects) => {
        const community = returnCommunity(subjects, user);
        const unique = getUniqueListBy(community.flat(), "id");
        setCommunity(unique);
        setFilteredFriends(unique);
        setSearch("");
      })
      .then(() => setLoading(false));
  }, []);

  const navigateTo = (item) => {
    returnConversations(user.id)
      .then((result) => result.data.getStudent.conversations.items)
      .then((conversations) => {
        if (conversations.length === 0) {
          createConversation(item, user).then((result) => {
            addUserToConversation(result.data.createConversation.id, item)
              .then(() =>
                addUserToConversation(result.data.createConversation.id, user)
              )
              .then(() => {
                navigation.navigate("Chat", {
                  thread: item,
                  conversationId: result.data.createConversation.id,
                  messages: [],
                });
              });
          });
        } else {
          conversations.map((obj) => {
            //if the recipient is a member of a conversation with the user, return their student object
            const students = obj.conversation.members.items.filter((member) => {
              return member.student.id === item.id;
            });

            if (students.length !== 0) {
              navigation.navigate("Chat", {
                thread: item,
                conversationId: obj.conversation.id,
                messages: obj.conversation.messages.items,
              });
            } else {
              createConversation(item, user).then((result) => {
                addUserToConversation(result.data.createConversation.id, item)
                  .then(() =>
                    addUserToConversation(
                      result.data.createConversation.id,
                      user
                    )
                  )
                  .then(() => {
                    navigation.navigate("Chat", {
                      thread: item,
                      conversationId: result.data.createConversation.id,
                      messages: [],
                    });
                  });
                return result;
              });
            }
          });
        }
      });
  };

  const searchFilterFunction = (text) => {
    if (text) {
      const newData = filteredFriends.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase();
        const friendName = text.toUpperCase();
        return itemData.indexOf(friendName) > -1;
      });
      setFilteredFriends(newData);
      setSearch(text);
    } else {
      setFilteredFriends(community);
      setSearch(text);
    }
  };

  //return data alphabetically
  filteredFriends.sort((a, b) =>
    a.name > b.name ? 1 : b.name > a.name ? -1 : 0
  );

  if (loading) {
    return <Loading />;
  } else {
    return (
      <SafeAreaView style={globalStyles.container}>
        <Searchbar
          style={globalStyles.searchBarStyles}
          placeholder="Search..."
          onChangeText={(text) => searchFilterFunction(text)}
          value={search}
        />
        <View style={styles.containerStyle}>
          <FlatList
            data={filteredFriends}
            keyExtractor={(item) => item.id.toString()}
            ItemSeparatorComponent={() => <Divider />}
            renderItem={({ item }) => {
              return (
                <ContactCard item={item} onPress={() => navigateTo(item)} />
              );
            }}
          />
        </View>
      </SafeAreaView>
    );
  }
};

export default NewDirectMessage;

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: "row",
  },
});
