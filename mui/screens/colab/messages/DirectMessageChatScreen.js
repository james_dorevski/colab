import React, { useEffect, useState } from "react";
import { Bubble, GiftedChat } from "react-native-gifted-chat";
import Colors from "../../../constants/Colors";
import { useUserContextState } from "../../../UserContext";
import { createMessage } from "../../../services/MessagesService";
import { defaultAvatars } from "../../../constants/DefaultAvatars";

export default function DirectMessageChatScreen({ route }) {
  const user = useUserContextState();
  const { conversationId, messages } = route.params;
  const [messageThread, setMessageThread] = useState([]);
  const userAvatar = defaultAvatars[user.avatar].uri;

  /*
  todo set up a subscription so messages are re-rendered each time they are updated for a conversation
  todo if a conversation between these two users already exists go to it.
   */
  useEffect(() => {
    let formattedThreads;
    if (messages.length !== 0) {
      formattedThreads = messages.map((message) => {
        return {
          _id: message.id,
          text: message.content,
          createdAt: message.createdAt,
          user: {
            _id: message.author.id,
            name: message.author.name,
            avatar: defaultAvatars[message.author.avatar].uri,
          },
        };
      });
      setMessageThread(
        formattedThreads.sort((a, b) => {
          return b.createdAt - a.createdAt;
        })
      );
    }
  }, []);

  // custom chat bubble styling
  function renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: Colors.rightChatBubble,
          },
          left: {
            backgroundColor: Colors.leftChatBubble,
          },
        }}
        textStyle={{
          right: {
            color: "#fff",
          },
        }}
      />
    );
  }

  // helper method that sends a message
  function handleSend(newMessage = []) {
    setMessageThread(GiftedChat.append(messageThread, newMessage));
    createMessage(newMessage[0], user, conversationId).then(
      () => {},
      (rejected) => {
        console.error(rejected);
      }
    );
  }

  return (
    <GiftedChat
      messages={messageThread.sort((a, b) => {
        let dateA = new Date(a.createdAt);
        let dateB = new Date(b.createdAt);
        return dateB - dateA;
      })}
      onSend={(newMessage) => handleSend(newMessage)}
      user={{ _id: user.id, avatar: userAvatar }}
      renderBubble={renderBubble}
      isTyping={true}
      alwaysShowSend={true}
      showUserAvatar
    />
  );
}
