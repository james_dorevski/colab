import React, { useState, useEffect } from "react";
import { FlatList, TouchableOpacity, SafeAreaView } from "react-native";
import { Divider, Searchbar } from "react-native-paper";
import Loading from "../../../components/Loading";
import globalStyles from "../../../styles/globalStyles";
import { ListItem } from "react-native-elements";

/*
TODO navigate to conversations
 */
export default function GroupChatsScreen() {
  const [threads, setThreads] = useState([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [filteredThreads, setFilteredThreads] = useState([]);

  /**
   * TODO: Fetch message threads from AppSync
   */
  useEffect(() => {
    //stubs
    let threads = [
      {
        _id: 1,
        name: "#ui-ux-discussion",
        latestMessage: {
          text: "What do you guys think of this mockup?",
        },
      },
      {
        _id: 2,
        name: "#documentation",
        latestMessage: {
          text: "I'm not sure... Maybe we should ask Mark about the spec?",
        },
      },
      {
        _id: 3,
        name: "#meetings",
        latestMessage: {
          text: "Next meeting is at 3pm tomorrow.",
        },
      },
      {
        _id: 4,
        name: "#devs-discussion",
        latestMessage: {
          text: "can someone investigate the use of graphql for this?",
        },
      },
      {
        _id: 5,
        name: "#random",
        latestMessage: {
          text: "check out this meme",
        },
      },
    ];

    setThreads(threads);
    setFilteredThreads(threads);
    setSearch("");

    if (loading) {
      setLoading(false);
    }
  }, []);

  if (loading) {
    return <Loading />;
  }

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = threads.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredThreads(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredThreads(threads);
      setSearch(text);
    }
  };

  return (
    <SafeAreaView style={globalStyles.container}>
      <Searchbar
        style={globalStyles.searchBarStyles}
        placeholder="Search..."
        onChangeText={(text) => searchFilterFunction(text)}
        value={search}
      />
      <FlatList
        data={filteredThreads}
        keyExtractor={(item) => item._id.toString()}
        ItemSeparatorComponent={() => <Divider />}
        renderItem={({ item }) => (
          <TouchableOpacity
          // onPress={() => navigation.navigate("Chat", { thread: item })}
          >
            <ListItem bottomDivider>
              <ListItem.Content>
                <ListItem.Title numberOfLines={1}>{item.name}</ListItem.Title>
                <ListItem.Subtitle numberOfLines={1}>
                  {item.latestMessage.text}
                </ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          </TouchableOpacity>
        )}
      />
    </SafeAreaView>
  );
}
