import React, { useState, useEffect } from "react";
import {
  FlatList,
  TouchableOpacity,
  View,
  Text,
  SectionList,
} from "react-native";
import { Searchbar } from "react-native-paper";
import Loading from "../../../components/Loading";
import globalStyles from "../../../styles/globalStyles";
import { groupsStub } from "../../../stubs/stubs";
import EmptyStateDashComponent from "../../../components/EmptyStateDashComponent";
import { Avatar, ListItem } from "react-native-elements";
import { useUserContextState } from "../../../UserContext";
import { returnConversations } from "../../../services/MessagesService";
import {
  defaultAvatars,
  defaultGroupAvatars,
} from "../../../constants/DefaultAvatars";

/*
TODO replace API calls with subscriptions
 */
export default function MessagesMainScreen({ navigation }) {
  const user = useUserContextState();

  const [threads, setThreads] = useState([]);
  const [groups, setGroups] = useState([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [filteredThreads, setFilteredThreads] = useState([]);

  useEffect(() => {
    returnConversations(user.id).then((result) => {
      if (result.data.getStudent.conversations.items !== 0) {
        setThreads(result.data.getStudent.conversations.items);
        setFilteredThreads(result.data.getStudent.conversations.items);
      }
    });
    /*
    TODO replace with API call and handle groups properly
     */
    // let defaultGroups;
    // user.groups != null && user.groups.length !== 0
    //   ? (defaultGroups = user.groups)
    //   : (defaultGroups = groupsStub);

    setGroups(groupsStub);
    setSearch("");

    if (loading) {
      setLoading(false);
    }
  }, []);

  const renderNoContent = ({ section }) => {
    if (section.data.length === 0) {
      return (
        <View style={globalStyles.emptyState}>
          <EmptyStateDashComponent
            text="Send a message to see it here."
            image={require("../../../../assets/emptyStateScreens/partners_30.png")}
          />
        </View>
      );
    }
    return null;
  };

  if (loading) {
    return <Loading />;
  }

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = threads.filter(function (item) {
        return item.conversation.messages.items.some((messageThread) => {
          const itemData = messageThread.author.name
            ? messageThread.author.name.toUpperCase()
            : "".toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
      });
      setFilteredThreads(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredThreads(threads);
      setSearch(text);
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "white",
        flexDirection: "column",
      }}
    >
      <SectionList
        ListHeaderComponent={
          <>
            <Searchbar
              style={globalStyles.searchBarStyles}
              placeholder="Search..."
              onChangeText={(text) => searchFilterFunction(text)}
              value={search}
            />
            {groups.length !== 0 ? (
              <>
                <FlatList
                  data={groups}
                  renderItem={({ item, index }) => {
                    return (
                      <TouchableOpacity
                        onPress={() =>
                          navigation.navigate("GroupChat", { thread: item })
                        }
                        key={index}
                      >
                        <View
                          style={{
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <View
                            style={{
                              alignItems: "center",
                              justifyContent: "center",
                              borderRadius: 5,
                              height: 70,
                              width: 70,
                            }}
                          >
                            <Avatar
                              source={{
                                uri: defaultGroupAvatars[item.avatar].uri,
                              }}
                              rounded
                              size={"medium"}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: "center",
                              maxWidth: 75,
                              fontSize: 13,
                            }}
                            numberOfLines={2}
                          >
                            {item.name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    );
                  }}
                  keyExtractor={(item) => item.id.toString()}
                  contentContainerStyle={{ padding: "1%" }}
                  ItemSeparatorComponent={() => (
                    <View style={{ width: "1%" }} />
                  )}
                  horizontal
                />
              </>
            ) : (
              //display empty state
              <View style={globalStyles.emptyState}>
                <EmptyStateDashComponent
                  text="Join a group to see it here."
                  image={require("../../../../assets/emptyStateScreens/groups_30.png")}
                />
              </View>
            )}
          </>
        }
        renderSectionFooter={renderNoContent}
        sections={[
          {
            title: "MessageThreads",
            data: filteredThreads,
            keyExtractor: (item, index) => item + index,
            renderItem: ({ item, index }) => {
              let sortedMessages = item.conversation.messages.items.sort(
                (a, b) => {
                  return new Date(b.createdAt) - new Date(a.createdAt);
                }
              );

              let messageRecipient = item.conversation.members.items.find(
                (student) => student.student.id !== user.id
              );

              let conversationId = item.conversation.id;

              return (
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate("Chat", {
                      thread: messageRecipient.student,
                      messages: item.conversation.messages.items,
                      conversationId: conversationId,
                    })
                  }
                  key={index}
                >
                  <ListItem bottomDivider>
                    <Avatar
                      source={{
                        uri:
                          defaultAvatars[messageRecipient.student.avatar].uri,
                      }}
                      size={"medium"}
                    />
                    <ListItem.Content>
                      <ListItem.Title numberOfLines={1}>
                        {messageRecipient.student.name}
                      </ListItem.Title>
                      <ListItem.Subtitle numberOfLines={1}>
                        {sortedMessages[0].content}
                      </ListItem.Subtitle>
                    </ListItem.Content>
                  </ListItem>
                </TouchableOpacity>
              );
            },
          },
        ]}
      />
    </View>
  );
}
