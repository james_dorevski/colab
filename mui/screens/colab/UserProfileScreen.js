import React from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  Modal,
  ScrollView,
} from "react-native";
import { Avatar } from "react-native-elements";
import { Button } from "react-native-paper";
import Colors from "../../constants/Colors";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";
import GroupCard from "../../components/GroupCard";
import { Auth } from "aws-amplify";
import Loading from "../../components/Loading";
import { groupsStub } from "../../stubs/stubs";
import { defaultAvatars } from "../../constants/DefaultAvatars";
import AvatarPicker from "../../components/AvatarPicker";
import { useUserContextState } from "../../UserContext";
import {
  getUserInformation,
  updateAvatar,
} from "../../services/UserInformationService";

const UserProfileScreen = ({ route, navigation }) => {
  const userLoggedIn = useUserContextState();

  const { userId } = route.params;

  const [loading, setLoading] = React.useState(true);
  const [user, setUser] = React.useState();
  const [avatarPickerVisible, setAvatarPickerVisible] = React.useState(false);

  React.useEffect(() => {
    getUserInformation(userId).then((result) => {
      setUser(result.student);
      setLoading(false);
    });
  }, [userId, avatarPickerVisible]);

  const showGroup = (item) => {
    navigation.navigate("Groups", {
      screen: "GroupProfile",
      params: { group: item },
      initial: false,
    });
  };

  if (loading === true) {
    return <Loading />;
  } else {
    return (
      <SafeAreaView style={styles.screen}>
        <View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={avatarPickerVisible}
            onRequestClose={() => setAvatarPickerVisible(false)}
            style={styles.modalContainer}
          >
            <ScrollView style={styles.modalContainer}>
              <View style={styles.modalView}>
                <AvatarPicker
                  value={userLoggedIn.avatar}
                  onValueSelect={(selectedAvatar) => {
                    userLoggedIn.setAvatar(selectedAvatar.id);
                  }}
                />
                <Button
                  style={styles.modalButton}
                  color={Colors.primaryColor}
                  onPress={() => {
                    updateAvatar().then(setAvatarPickerVisible(false));
                  }}
                >
                  SET
                </Button>
              </View>
            </ScrollView>
          </Modal>
        </View>
        <FlatList
          ListHeaderComponent={
            <>
              <View style={styles.userDetails}>
                <TouchableOpacity>
                  {/* Hide edit button if not on your profile */}
                  {userId === Auth.user.attributes.sub ? (
                    <>
                      <Avatar
                        size={140}
                        source={{
                          uri: defaultAvatars[userLoggedIn.avatar].uri,
                        }}
                        rounded
                      />
                      <Avatar.Accessory
                        size={26}
                        onPress={() => setAvatarPickerVisible(true)}
                      />
                    </>
                  ) : (
                    <Avatar
                      size={140}
                      source={{ uri: defaultAvatars[user.avatar].uri }}
                      rounded
                    />
                  )}
                </TouchableOpacity>
                <Text style={styles.username}>{user.name}</Text>
                <Text style={styles.userCampusAndAverage}>
                  {user.university.abbreviation} | {user.campus.name} Campus
                </Text>
                <Text style={styles.userCampusAndAverage}>
                  {user.preferredLanguage}
                </Text>
                {/* Display either edit profile or message user depending on if the user is on their profile */}
                {userId === Auth.user.attributes.sub ? (
                  <Button
                    mode="contained"
                    style={styles.editProfileButton}
                    onPress={() => navigation.navigate("AccountSettings")}
                  >
                    <Text style={styles.buttonText}>Edit Profile</Text>
                  </Button>
                ) : (
                  <Button mode="contained" style={styles.MessageUserButton}>
                    <Text style={styles.buttonText}>Message</Text>
                  </Button>
                )}
              </View>

              <View style={styles.statsContainer}>
                <View style={styles.stats}>
                  <MaterialIcons name="timelapse" size={28} />
                  <Text style={styles.statsNumber}>0</Text>
                  <Text style={styles.statsName}>Hours studied</Text>
                </View>

                <View style={styles.stats}>
                  <Ionicons name="md-wifi" size={28} />
                  <Text style={styles.statsNumber}>0</Text>
                  <Text style={styles.statsName}>Beacons sent</Text>
                </View>

                <View style={styles.stats}>
                  <MaterialIcons name="people-outline" size={28} />
                  <Text style={styles.statsNumber}>0</Text>
                  <Text style={styles.statsName}>Groups joined</Text>
                </View>

                <View style={styles.stats}>
                  <Ionicons name="md-book" size={28} />
                  <Text style={styles.statsNumber}>0</Text>
                  <Text style={styles.statsName}>Study sessions</Text>
                </View>
              </View>

              <View style={styles.profileSection}>
                <Text style={{ fontFamily: "raleway", fontSize: 20 }}>
                  Groups
                </Text>
                {/* <Button
                  mode="text"
                  onPress={() =>
                    navigation.navigate("Groups", {
                      screen: "ShowAllUserGroups",
                      params: { user: user },
                    })
                  }
                >
                  <Text style={styles.showAllButtonText}>Show All</Text>
                </Button> */}
              </View>
            </>
          }
          ListHeaderComponentStyle={{
            marginHorizontal: "5%",
            marginVertical: "2%",
          }}
          ListFooterComponent={<View style={{ marginBottom: "5%" }} />}
          data={groupsStub}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => {
            if (index < 4) {
              return (
                <View style={{ marginHorizontal: "5%", alignItems: "center" }}>
                  <GroupCard group={item} onPress={() => showGroup(item)} />
                </View>
              );
            }
          }}
          ItemSeparatorComponent={() => <View style={{ marginBottom: "3%" }} />}
        />
      </SafeAreaView>
    );
  }
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  userDetails: {
    marginVertical: "5%",
    alignItems: "center",
  },
  username: {
    fontFamily: "raleway-bold",
    fontSize: 24,
    marginTop: "3%",
  },
  userCampusAndAverage: {
    fontFamily: "raleway",
    fontSize: 14,
    marginTop: "1%",
  },
  editProfileButton: {
    marginTop: "3%",
    backgroundColor: "grey",
  },
  MessageUserButton: {
    marginTop: "3%",
    backgroundColor: Colors.primaryColor,
  },
  buttonText: {
    fontSize: 10,
    fontFamily: "raleway-bold",
  },
  profileSection: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: "5%",
  },
  showAllButtonText: {
    color: Colors.secondaryColor,
  },
  statsContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: "5%",
  },
  stats: {
    alignItems: "center",
    marginHorizontal: "3%",
    alignSelf: "flex-end",
  },
  statsNumber: {
    fontFamily: "raleway-medium",
    fontSize: 30,
    color: Colors.secondaryColor,
  },
  statsName: {
    fontFamily: "raleway-bold",
    fontSize: 10,
  },
  modalContainer: {
    flex: 1,
    alignSelf: "center",
    width: "98%",
    marginVertical: "10%",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButton: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
});

export default UserProfileScreen;
