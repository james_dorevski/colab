import React, { useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import globalStyles from "../../styles/globalStyles";
import { Avatar } from "react-native-elements";
import { useUserContextState } from "../../UserContext";
import NotificationsDashComponent from "../../components/NotificationsDashComponent";
import GroupsDashComponent from "../../components/GroupsDashComponent";
import StudyPartnersDashComponent from "../../components/StudyPartnersDashComponent";
import OnboardingIncompleteDashComponent from "../../components/OnboardingIncompleteDashComponent";
import { useOnboardingContextState } from "../onboarding/OnboardingContext";
import { defaultAvatars } from "../../constants/DefaultAvatars";
import Loading from "../../components/Loading";
import { getUserInformation } from "../../services/UserInformationService";
import { Auth } from "aws-amplify";

const DashboardScreen = ({ navigation }) => {
  const user = useUserContextState();
  const onboardingState = useOnboardingContextState();

  const [loading, setLoading] = React.useState(true);

  useEffect(() => {
    getUserInformation(Auth.user.attributes.sub).then((results) => {
      user.setId(results.student.id);
      user.setName(results.student.name);
      user.setAvatar(results.student.avatar);
      user.setWam(results.student.wam);
      user.setPreferredLanguage(results.student.preferredLanguage);
      user.setCampus(results.student.campus.name);
      user.setUniversity(results.student.university.name);
      user.setGroups(results.student.groups.items);
      user.setSubjects(results.student.subjects.items);
      user.setSubjectCodes(
        results.student.subjects.items.map((item) => item.subject.code)
      );
      user.setExistingSubjectIds(
        results.student.subjects
          ? results.student.subjects.items.map((item) => item.id)
          : []
      );
      if (results.student.messages) {
        user.setMessages(results.student.messages.items);
      }

      results.groups
        ? user.setSuggestedGroups(results.groups)
        : user.setSuggestedGroups("fail");

      setLoading(false);
    });
    setOnboardingComplete();
  }, []);

  // display correct time of day in welcome text
  let timeOfDay;
  if (new Date().getHours() < 12) {
    timeOfDay = "morning";
  } else if (new Date().getHours() < 18) {
    timeOfDay = "afternoon";
  } else {
    timeOfDay = "evening";
  }

  // sets onboarding to complete if user has filled in all onboarding info
  const setOnboardingComplete = () => {
    onboardingState.wam &&
    onboardingState.campus &&
    onboardingState.avatar &&
    onboardingState.subjects &&
    onboardingState.availability !== []
      ? onboardingState.setIsComplete(true)
      : null;
  };

  const showGroup = (item) => {
    navigation.navigate("Groups", {
      screen: "GroupProfile",
      params: { group: item },
      initial: false,
    });
  };

  const showUser = (item) => {
    navigation.navigate("UserProfile", { userId: item.id });
  };

  if (loading === true) {
    return <Loading />;
  } else {
    return (
      <SafeAreaView style={globalStyles.androidSafeArea}>
        <ScrollView>
          <View style={styles.header}>
            <Image
              source={require("../../../assets/colab.png")}
              style={styles.image}
              resizeMode="contain"
            />
            <TouchableOpacity
              style={styles.userAvatar}
              onPress={() => navigation.navigate("Settings")}
            >
              <Avatar
                size={50}
                source={{ uri: defaultAvatars[user.avatar].uri }}
                rounded
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.welcomeText}>
            Good {timeOfDay}, {user.name}
          </Text>
          <View>
            <OnboardingIncompleteDashComponent
              onboardingIncomplete={!onboardingState.isComplete}
            />
          </View>
          <View>
            <NotificationsDashComponent />
          </View>
          <View>
            <GroupsDashComponent
              navigateTo={() =>
                navigation.navigate("Groups", { screen: "GroupsMain" })
              }
              showGroups={showGroup}
            />
          </View>
          <View>
            <StudyPartnersDashComponent
              navigateTo={() => navigation.navigate("SearchUsers")}
              showUsers={showUser}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    marginTop: "10%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  image: {
    marginLeft: "5%",
  },
  userAvatar: {
    marginRight: "5%",
  },
  welcomeText: {
    fontFamily: "raleway-bold",
    fontSize: 24,
    marginHorizontal: "5%",
    marginTop: "4%",
  },
});

export default DashboardScreen;
