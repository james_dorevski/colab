import React from "react";
import { View, Text, StyleSheet, FlatList, SafeAreaView } from "react-native";
import PropTypes from "prop-types";
import { Searchbar } from "react-native-paper";
import ContactCard from "../../components/ContactCard";
import Loading from "../../components/Loading";
import { useUserContextState } from "../../UserContext";
import globalStyles from "../../styles/globalStyles";
import { defaultAvatars } from "../../constants/DefaultAvatars";
import {
  getUniqueListBy,
  returnCommunity,
  returnUserSubjects,
} from "../../services/CommunityService";

const SearchUsersScreen = ({ navigation }) => {
  const user = useUserContextState();

  const [students, setStudents] = React.useState([]);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [filteredSearchResults, setFilteredSearchResults] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    setLoading(true);
    returnUserSubjects()
      .then((subjects) => {
        return returnCommunity(subjects, user);
      })
      .then((community) => {
        return getUniqueListBy(community.flat(), "id");
      })
      .then((unique) => {
        setStudents(unique);
        setFilteredSearchResults(unique);
        setSearchQuery("");
        setLoading(false);
      });
  }, []);

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = students.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredSearchResults(newData);
      setSearchQuery(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredSearchResults(students);
      setSearchQuery(text);
    }
  };

  if (loading) {
    return <Loading />;
  } else {
    return (
      <SafeAreaView style={styles.screen}>
        <Searchbar
          placeholder="Search"
          onChangeText={(text) => searchFilterFunction(text)}
          value={searchQuery}
          style={globalStyles.searchBarStyles}
        />
        <Text style={styles.largeText}>Results</Text>
        <FlatList
          data={filteredSearchResults}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => {
            return (
              <ContactCard
                item={item}
                avatar={{ uri: defaultAvatars[item.avatar].uri }}
                onPress={() =>
                  navigation.navigate("UserProfile", { userId: item.id })
                }
              />
            );
          }}
          ItemSeparatorComponent={() => <View style={{ marginTop: "3%" }} />}
          contentContainerStyle={{ marginTop: "1%" }}
        />
      </SafeAreaView>
    );
  }
};
SearchUsersScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  largeText: {
    fontFamily: "raleway-bold",
    fontSize: 20,
    marginHorizontal: "5%",
    marginTop: "5%",
    marginBottom: "3%",
  },
});

export default SearchUsersScreen;
