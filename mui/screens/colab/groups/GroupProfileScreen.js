import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  SectionList,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { Button, Card } from "react-native-paper";
import Colors from "../../../constants/Colors";
import ScheduleCard from "../../../components/ScheduleCard";
import { scheduleStub, groupThreadStub } from "../../../stubs/stubs";
import {
  defaultAvatars,
  defaultGroupAvatars,
} from "../../../constants/DefaultAvatars";
import { useUserContextState } from "../../../UserContext";

/*
TODO add join group functionality
 */
const GroupProfileScreen = ({ route }) => {
  const { group } = route.params;
  const user = useUserContextState();
  const [members, setMembers] = useState(group.students.items);
  const [joined, setJoined] = useState(
    members.filter((member) => member.student.id === user.id).length === 1
  );

  const styles = StyleSheet.create({
    containerStyle: {
      flex: 1,
      width: "95%",
    },
    text: {
      color: "white",
      textAlign: "center",
      fontSize: 30,
      fontWeight: "bold",
    },
    row: {
      flex: 1,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "flex-start",
    },
    subContainer: {
      flex: 1,
      backgroundColor: "#fff",
      width: "20%",
    },
    groupsRendererCard: {
      marginTop: "2%",
      width: "95%",
      flex: 1,
      flexDirection: "column",
      alignItems: "center",
      alignSelf: "center",
      borderRadius: 5,
    },
    groupNameText: {
      fontFamily: "raleway-bold",
      fontSize: 24,
      marginVertical: "3%",
    },
    headingText: {
      fontFamily: "raleway",
      fontSize: 20,
      marginTop: "4%",
    },
    topButton: {
      width: 120,
    },
    topButtonText: {
      fontFamily: "raleway-bold",
      fontSize: 10,
    },
    subText: {
      color: "#454545",
      marginTop: "1%",
    },
    reportButtonContainer: {
      alignItems: "flex-end",
      marginTop: "10%",
      marginBottom: "2%",
    },
  });

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
      <View style={{ marginHorizontal: "5%" }}>
        <SectionList
          ListHeaderComponent={
            <>
              <View style={styles.groupsRendererCard}>
                <View
                  style={{
                    borderWidth: 5,
                    borderColor: Colors.secondaryColor,
                    borderRadius: 125 / 5,
                  }}
                >
                  <Image
                    source={{ uri: defaultGroupAvatars[group.avatar].uri }}
                    style={{ width: 125, height: 125, borderRadius: 20 }}
                  />
                </View>
                <Text style={styles.groupNameText}>{group.name}</Text>
                <View style={{ marginTop: "2%" }}>
                  {joined ? (
                    <Button
                      mode="outlined"
                      color={Colors.secondaryColor}
                      style={styles.topButton}
                      onPress={() => {
                        setJoined(false);
                        setMembers(
                          members.filter(
                            (member) => member.student.id !== user.id
                          )
                        );
                      }}
                    >
                      <Text style={styles.topButtonText}>LEAVE GROUP</Text>
                    </Button>
                  ) : (
                    <Button
                      mode="contained"
                      color={Colors.secondaryColor}
                      style={styles.topButton}
                      onPress={() => {
                        setJoined(true);
                        setMembers([
                          ...members,
                          {
                            student: {
                              name: user.name,
                              avatar: user.avatar,
                              id: user.id,
                            },
                          },
                        ]);
                      }}
                    >
                      <Text style={styles.topButtonText}>JOIN</Text>
                    </Button>
                  )}
                </View>
              </View>
              <View style={styles.row}>
                <View style={styles.subContainer}>
                  <Text style={styles.headingText}>Members</Text>
                </View>
              </View>
              <FlatList
                data={members}
                renderItem={({ item }) => {
                  return (
                    <View
                      style={{
                        alignItems: "center",
                        justifyContent: "flex-start",
                      }}
                    >
                      <View
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          borderRadius: 5,
                          height: 70,
                          width: 70,
                        }}
                      >
                        <Image
                          source={{
                            uri: defaultAvatars[item.student.avatar].uri,
                          }}
                          style={{ width: 60, height: 60, borderRadius: 30 }}
                        />
                      </View>
                      <Text
                        style={{ textAlign: "center", maxWidth: 75 }}
                        numberOfLines={2}
                      >
                        {item.student.name}
                      </Text>
                    </View>
                  );
                }}
                keyExtractor={(item) => item.student.id.toString()}
                contentContainerStyle={{ padding: "1%" }}
                horizontal
              />
              <View style={styles.row}>
                <View style={styles.subContainer}>
                  <Text style={styles.headingText}>Description</Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={styles.containerStyle}>
                  <Text style={styles.subText}>{group.description}</Text>
                </View>
              </View>
            </>
          }
          ListFooterComponent={
            <View style={styles.reportButtonContainer}>
              <Button color={Colors.secondaryColor}>REPORT GROUP</Button>
            </View>
          }
          renderSectionHeader={({ section: { title } }) => (
            <Text style={styles.headingText}>{title}</Text>
          )}
          keyExtractor={(item, index) => item + index}
          sections={[
            {
              title: "Schedule",
              data: scheduleStub,
              renderItem: ({ item }) => <ScheduleCard group={item} />,
            },
            {
              title: "Chats",
              data: groupThreadStub,
              renderItem: ({ item }) => (
                <TouchableOpacity
                  style={{
                    marginVertical: "1%",
                    marginHorizontal: "1%",
                  }}
                >
                  <Card
                    style={{
                      height: 50,
                      backgroundColor: Colors.fafafa,
                    }}
                  >
                    <Text
                      style={{
                        marginVertical: "3%",
                        marginHorizontal: "4%",
                        fontSize: 16,
                      }}
                    >
                      #{item.name}
                    </Text>
                  </Card>
                </TouchableOpacity>
              ),
            },
          ]}
        />
      </View>
    </SafeAreaView>
  );
};

export default GroupProfileScreen;
