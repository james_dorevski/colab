import React, { useState, useEffect } from "react";
import { StyleSheet, FlatList, SafeAreaView, View, Text } from "react-native";
import { Searchbar } from "react-native-paper";
import Loading from "../../../components/Loading";
import globalStyles from "../../../styles/globalStyles";
import { groupsStub } from "../../../stubs/stubs";
import EmptyStateDashComponent from "../../../components/EmptyStateDashComponent";
import { Button } from "react-native-paper";
import { useUserContextState } from "../../../UserContext";
import { API, Auth, graphqlOperation } from "aws-amplify";
import * as queries from "../../../../src/graphql/custom-queries";
import GroupCard from "../../../components/GroupCard";
import Colors from "../../../constants/Colors";

export default function GroupsMainScreen({ navigation }) {
  const user = useUserContextState();

  const [groups, setGroups] = useState([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [filteredGroups, setFilteredGroups] = useState([]);

  const setUserDetails = async () => {
    const result = await API.graphql(
      graphqlOperation(queries.getStudent, { id: Auth.user.attributes.sub })
    );

    return result.data.getStudent.groups.items;
  };

  useEffect(() => {
    //stubs
    setUserDetails().then((groupObjects) => {
      if (groupObjects === undefined || !groupObjects) {
        setGroups(groupsStub);
        setFilteredGroups(groupsStub);
      } else {
        user.setGroups(groupObjects);
        setGroups(groupObjects);
        setFilteredGroups(groupObjects);
      }
    });

    setSearch("");

    if (loading) {
      setLoading(false);
    }
  }, []);

  if (loading) {
    return <Loading />;
  }

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = groups.filter(function (item) {
        const itemData = item.group.name
          ? item.group.name.toUpperCase()
          : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredGroups(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredGroups(groups);
      setSearch(text);
    }
  };

  const buttons = () => {
    return (
      <View style={styles.buttonContainer}>
        <Button
          mode="text"
          color={Colors.secondaryColor}
          onPress={() => navigation.navigate("CreateGroup")}
          style={styles.createGroupButton}
        >
          <Text style={styles.laterButtonText}>CREATE GROUP</Text>
        </Button>
        <Button
          mode="contained"
          type="submit"
          color={Colors.secondaryColor}
          onPress={() => navigation.navigate("FindGroup")}
          style={styles.findGroupButton}
        >
          <Text style={styles.nextButtonText}>FIND GROUPS</Text>
        </Button>
      </View>
    );
  };

  return (
    <SafeAreaView style={globalStyles.container}>
      {groups.length > 0 ? (
        <FlatList
          ListHeaderComponent={
            <Searchbar
              style={globalStyles.searchBarStyles}
              placeholder="Search"
              onChangeText={(text) => searchFilterFunction(text)}
              value={search}
            />
          }
          ListHeaderComponentStyle={{ marginBottom: "5%" }}
          ListFooterComponent={buttons()}
          data={filteredGroups}
          renderItem={({ item, index }) => {
            return (
              <View style={{ marginHorizontal: "5%", alignItems: "center" }}>
                <GroupCard
                  group={item.group}
                  key={index}
                  onPress={() =>
                    navigation.navigate("GroupProfile", { group: item.group })
                  }
                />
              </View>
            );
          }}
          keyExtractor={(item) => item.id.toString()}
          ItemSeparatorComponent={() => <View style={{ marginTop: "3%" }} />}
        />
      ) : (
        <>
          <View style={globalStyles.emptyState}>
            <EmptyStateDashComponent
              text="Join a group to see it here."
              image={require("../../../../assets/emptyStateScreens/groups_30.png")}
            />
          </View>
          {buttons()}
        </>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: "column-reverse",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: "10%",
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  findGroupButton: {
    height: 45,
    width: 190,
    justifyContent: "center",
    alignItems: "center",
  },
  createGroupButton: {
    height: 45,
    width: 180,
    justifyContent: "center",
    alignItems: "center",
    marginTop: "2%",
  },
});
