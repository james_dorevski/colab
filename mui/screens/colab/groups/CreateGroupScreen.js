import React from "react";
import { StyleSheet } from "react-native";
import ScrollableTabView, {
  ScrollableTabBar,
} from "react-native-scrollable-tab-view";
import { useUserContextState } from "../../../UserContext";
import Colors from "../../../constants/Colors";
import CreateGroupTabComponent from "../../../components/CreateGroupTabComponent";

const CreateGroupScreen = () => {
  const user = useUserContextState();

  return (
    <ScrollableTabView
      tabBarBackgroundColor="white"
      tabBarActiveTextColor={Colors.secondaryColor}
      tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
      renderTabBar={() => <ScrollableTabBar />}
    >
      {user.subjects.map((tab) => (
        <CreateGroupTabComponent
          key={tab.subject.code}
          tabLabel={tab.subject.code}
          currentSubject={tab.subject}
        />
      ))}
    </ScrollableTabView>
  );
};

const styles = StyleSheet.create({
  tabBarUnderlineStyle: {
    backgroundColor: Colors.secondaryColor,
  },
});

export default CreateGroupScreen;
