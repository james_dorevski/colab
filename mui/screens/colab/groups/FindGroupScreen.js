import React from "react";
import { StyleSheet, View } from "react-native";
import ScrollableTabView, {
  ScrollableTabBar,
} from "react-native-scrollable-tab-view";
import FindGroupTabComponent from "../../../components/FindGroupTabComponent";
import { useUserContextState } from "../../../UserContext";
import Colors from "../../../constants/Colors";
import EmptyStateDashComponent from "../../../components/EmptyStateDashComponent";
import globalStyles from "../../../styles/globalStyles";

const FindGroupScreen = ({ navigation }) => {
  const user = useUserContextState();

  const showGroup = (item) => {
    navigation.navigate("GroupProfile", { group: item });
  };

  if (user.subjects.length < 1) {
    return (
      <View style={globalStyles.emptyState}>
        <EmptyStateDashComponent
          text="Add your subjects so you can find groups for them!"
          image={require("../../../../assets/emptyStateScreens/groups.png")}
        />
      </View>
    );
  } else {
    return (
      <ScrollableTabView
        tabBarBackgroundColor="white"
        tabBarActiveTextColor={Colors.secondaryColor}
        tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
        renderTabBar={() => <ScrollableTabBar />}
      >
        {user.subjects.map((tab) => (
          <FindGroupTabComponent
            key={tab.subject.code}
            tabLabel={tab.subject.code}
            currentSubject={tab.subject}
            showGroups={showGroup}
            potentialGroups={user.suggestedGroups}
          />
        ))}
      </ScrollableTabView>
    );
  }
};

const styles = StyleSheet.create({
  tabBarUnderlineStyle: {
    backgroundColor: Colors.secondaryColor,
  },
});

export default FindGroupScreen;
