import React from "react";
import { Image, StyleSheet, Dimensions } from "react-native";

const BeaconMainScreen = () => {
  //TODO replace screen with feature once available
  const windowWidth = Dimensions.get("window").width;
  const styles = StyleSheet.create({
    backgroundImage: {
      width: windowWidth,
      flex: 1,
      resizeMode: "cover",
    },
  });
  return (
    <Image
      source={require("../../../../assets/backgroundImages/new-feature.png")}
      style={styles.backgroundImage}
    />
  );
};

export default BeaconMainScreen;
