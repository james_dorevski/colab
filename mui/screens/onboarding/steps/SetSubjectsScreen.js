import React from "react";
import {
  View,
  StyleSheet,
  Image,
  Text,
  SafeAreaView,
  Alert,
} from "react-native";
import Colors from "../../../constants/Colors";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import { useOnboardingContextState } from "../OnboardingContext";
import globalStyles from "../../../styles/globalStyles";
import { API, graphqlOperation } from "aws-amplify";
import * as queries from "../../../../src/graphql/custom-queries";
import Loading from "../../../components/Loading";
import { MaterialIcons } from "@expo/vector-icons";

const SetSubjectsScreen = () => {
  const onboardingState = useOnboardingContextState();
  const [loading, setLoading] = React.useState(true);
  const [items, setItems] = React.useState([
    {
      name: "See All",
      id: 0,
      children: [],
    },
  ]);

  React.useEffect(() => {
    setLoading(true);
    getAllSubjects()
      .then((result) => {
        setItems([
          {
            name: "See All",
            id: 0,
            children: result.data.listSubjects.items.map((subject) => {
              return {
                name: `${subject.code} ${subject.name}`,
                id: subject.id,
              };
            }),
          },
        ]);
      })
      .then(() => setLoading(false));
  }, []);

  async function getAllSubjects() {
    setLoading(true);
    try {
      return await API.graphql(graphqlOperation(queries.listSubjects));
    } catch (err) {
      Alert.alert("Error Retrieving Subjects", err.message, [{ text: "Ok" }]);
    }
  }

  return loading ? (
    <Loading />
  ) : (
    <SafeAreaView style={globalStyles.androidSafeArea}>
      <View style={styles.screen}>
        <Image
          source={require("../../../../assets/colab.png")}
          style={styles.image}
          resizeMode="contain"
        />
        <Text style={styles.text}>
          Add your subjects so we can find you some study partners!
        </Text>
        <View>
          <SectionedMultiSelect
            items={items}
            uniqueKey="id"
            subKey="children"
            selectText="Select your subjects"
            searchPlaceholderText="Search subjects..."
            showDropDowns={true}
            readOnlyHeadings={true}
            selectedItems={[...new Set(onboardingState.subjects)]}
            onSelectedItemsChange={(selectedSubjects) => {
              onboardingState.setSubjects([...selectedSubjects]);
            }}
            styles={{
              button: { backgroundColor: Colors.primaryColor },
              container: { marginVertical: "35%" },
            }}
            loading={loading}
            IconRenderer={MaterialIcons}
            selectLabelNumberOfLines={1}
            itemNumberOfLines={1}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
});
export default SetSubjectsScreen;
