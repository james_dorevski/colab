import React from "react";
import { View, StyleSheet, Text, Image, SafeAreaView } from "react-native";
import ChipPicker from "../../../components/ChipPicker";
import { useOnboardingContextState } from "../OnboardingContext";
import globalStyles from "../../../styles/globalStyles";

const SetCampusScreen = () => {
  const onboardingState = useOnboardingContextState();

  const campusArray = [
    {
      id: 1,
      name: "Wollongong",
    },
    {
      id: 2,
      name: "SWS",
    },
    {
      id: 3,
      name: "Shoalhaven",
    },
    {
      id: 4,
      name: "Innovation",
    },
  ];

  return (
    <SafeAreaView style={globalStyles.androidSafeArea}>
      <View style={styles.screen}>
        <Image
          source={require("../../../../assets/colab.png")}
          style={styles.image}
          resizeMode="contain"
        />
        <Text style={styles.text}>So, you&apos;re a UOW student?</Text>
        <Text style={styles.text}>Which is your campus?</Text>

        <ChipPicker
          items={campusArray}
          value={onboardingState.campus.name}
          onValueSelect={(selectedCampus) => {
            onboardingState.setCampus(selectedCampus);
          }}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
});

export default SetCampusScreen;
