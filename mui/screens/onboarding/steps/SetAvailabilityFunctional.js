import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
} from "react-native";
import { useOnboardingContextState } from "../OnboardingContext";
import ScrollableTabView, {
  ScrollableTabBar,
} from "react-native-scrollable-tab-view";
import { AntDesign } from "@expo/vector-icons";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import globalStyles from "../../../styles/globalStyles";
import Colors from "../../../constants/Colors";
import { Card } from "react-native-shadow-cards";

const SetAvailabilityFunctional = () => {
  const onboardingState = useOnboardingContextState();

  function makeId(length) {
    let result = "";
    let characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  const [mondayTimes, setMondayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Monday"),
  ]);
  const [tuesdayTimes, setTuesdayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Tuesday"),
  ]);
  const [wednesdayTimes, setWednesdayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Wednesday"),
  ]);
  const [thursdayTimes, setThursdayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Thursday"),
  ]);
  const [fridayTimes, setFridayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Friday"),
  ]);
  const [saturdayTimes, setSaturdayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Saturday"),
  ]);
  const [sundayTimes, setSundayTimes] = useState([
    ...onboardingState.availability.filter((item) => item.day === "Sunday"),
  ]);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [datePickerSource, setDatePickerSource] = useState({
    day: "",
    id: "",
    startOrEnd: "",
  });

  const [studentAvailability, setStudentAvailability] = useState([
    {
      day: "Monday",
      times: mondayTimes,
    },
    {
      day: "Tuesday",
      times: tuesdayTimes,
    },
    {
      day: "Wednesday",
      times: wednesdayTimes,
    },
    {
      day: "Thursday",
      times: thursdayTimes,
    },
    {
      day: "Friday",
      times: fridayTimes,
    },
    {
      day: "Saturday",
      times: saturdayTimes,
    },
    {
      day: "Sunday",
      times: sundayTimes,
    },
  ]);

  const refreshAvailabilityStub = () => {
    return [
      {
        day: "Monday",
        times: mondayTimes,
      },
      {
        day: "Tuesday",
        times: tuesdayTimes,
      },
      {
        day: "Wednesday",
        times: wednesdayTimes,
      },
      {
        day: "Thursday",
        times: thursdayTimes,
      },
      {
        day: "Friday",
        times: fridayTimes,
      },
      {
        day: "Saturday",
        times: saturdayTimes,
      },
      {
        day: "Sunday",
        times: sundayTimes,
      },
    ];
  };

  const defaultTimeObject = {
    id: makeId(16),
    start: "12:00am",
    end: "12:00am",
  };

  const addElement = async (daystr) => {
    let newObj;

    if (daystr === "Monday") {
      newObj = [...mondayTimes, { name: "Monday", ...defaultTimeObject }];
      setMondayTimes(newObj);
    } else if (daystr === "Tuesday") {
      newObj = [...tuesdayTimes, { name: "Tuesday", ...defaultTimeObject }];
      setTuesdayTimes(newObj);
    } else if (daystr === "Wednesday") {
      newObj = [...wednesdayTimes, { name: "Wednesday", ...defaultTimeObject }];
      setWednesdayTimes(newObj);
    } else if (daystr === "Thursday") {
      newObj = [...thursdayTimes, { name: "Thursday", ...defaultTimeObject }];
      setThursdayTimes(newObj);
    } else if (daystr === "Friday") {
      newObj = [...fridayTimes, { name: "Friday", ...defaultTimeObject }];
      setFridayTimes(newObj);
    } else if (daystr === "Saturday") {
      newObj = [...saturdayTimes, { name: "Saturday", ...defaultTimeObject }];
      setSaturdayTimes(newObj);
    } else if (daystr === "Sunday") {
      newObj = [...sundayTimes, { name: "Sunday", ...defaultTimeObject }];
      setSundayTimes(newObj);
    }
    refreshAvailabilityState().then(
      onboardingState.setAvailability(
        [
          mondayTimes,
          tuesdayTimes,
          wednesdayTimes,
          thursdayTimes,
          fridayTimes,
          saturdayTimes,
          sundayTimes,
        ].flat()
      )
    );
  };

  const removeCard = async (day, id) => {
    let filtered;
    if (day === "Monday") {
      filtered = mondayTimes.filter((value) => value.id !== id);
      setMondayTimes(filtered);
    } else if (day === "Tuesday") {
      filtered = tuesdayTimes.filter((value) => value.id !== id);
      setTuesdayTimes(filtered);
    } else if (day === "Wednesday") {
      filtered = wednesdayTimes.filter((value) => value.id !== id);
      setWednesdayTimes(filtered);
    } else if (day === "Thursday") {
      filtered = thursdayTimes.filter((value) => value.id !== id);
      setThursdayTimes(filtered);
    } else if (day === "Friday") {
      filtered = fridayTimes.filter((value) => value.id !== id);
      setFridayTimes(filtered);
    } else if (day === "Saturday") {
      filtered = saturdayTimes.filter((value) => value.id !== id);
      setSaturdayTimes(filtered);
    } else if (day === "Sunday") {
      filtered = sundayTimes.filter((value) => value.id !== id);
      setSundayTimes(filtered);
    }

    refreshAvailabilityState().then(
      onboardingState.setAvailability(
        [
          mondayTimes,
          tuesdayTimes,
          wednesdayTimes,
          thursdayTimes,
          fridayTimes,
          saturdayTimes,
          sundayTimes,
        ].flat()
      )
    );
  };

  const showDatePicker = (day, id, position) => {
    let newObj = {
      day: day,
      id: id,
      startOrEnd: position,
    };
    setDatePickerSource(newObj);
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const refreshAvailabilityState = async () => {
    setStudentAvailability(refreshAvailabilityStub());
  };

  const handleConfirm = (datetime) => {
    let hours = datetime.getHours();
    let mins = datetime.getMinutes();
    let mornOrArv = "am";
    if (mins < 10) {
      mins = ":0" + mins;
    } else {
      mins = ":" + mins;
    }
    if (hours > 12) {
      hours -= 12;
      mornOrArv = "pm";
    } else if (hours === 12) {
      mornOrArv = "pm";
    }

    let time = hours + mins + mornOrArv;
    hideDatePicker();
    editTimeslot(datePickerSource.day, time);
  };

  const editTimeslot = (sourceDay, time) => {
    let objIndex;
    switch (sourceDay) {
      case "Monday":
        objIndex = mondayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          mondayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          mondayTimes[objIndex].end = time;
        }
        break;
      case "Tuesday":
        objIndex = tuesdayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          tuesdayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          tuesdayTimes[objIndex].end = time;
        }
        break;
      case "Wednesday":
        objIndex = wednesdayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          wednesdayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          wednesdayTimes[objIndex].end = time;
        }
        break;
      case "Thursday":
        objIndex = thursdayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          thursdayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          thursdayTimes[objIndex].end = time;
        }
        break;
      case "Friday":
        objIndex = fridayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          fridayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          fridayTimes[objIndex].end = time;
        }
        break;
      case "Saturday":
        objIndex = saturdayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          saturdayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          saturdayTimes[objIndex].end = time;
        }
        break;
      case "Sunday":
        objIndex = sundayTimes.findIndex(
          (timeslot) => timeslot.id === datePickerSource.id
        );
        if (datePickerSource.startOrEnd === "start") {
          sundayTimes[objIndex].start = time;
        } else if (datePickerSource.startOrEnd === "end") {
          sundayTimes[objIndex].end = time;
        }
        break;
    }
    refreshAvailabilityState().then(
      onboardingState.setAvailability(
        [
          mondayTimes,
          tuesdayTimes,
          wednesdayTimes,
          thursdayTimes,
          fridayTimes,
          saturdayTimes,
          sundayTimes,
        ].flat()
      )
    );
  };

  const availToString = (timestring) => {
    const n = 2;
    if (!timestring) return;
    if (timestring.substring(timestring.length - n) === "am") {
      if (timestring.substring(1, 2) === ":") {
        return "0" + timestring.substring(0, 4);
      } else if (timestring.substring(0, 2) === "12") {
        return "00" + timestring.substring(2, 5);
      } else {
        return timestring.substring(0, 5);
      }
    } else {
      let formattedTime = "";
      let mins = "";
      let hours = "";

      if (timestring.substring(1, 2) === ":") {
        hours = timestring.substring(0, 1);
        mins = timestring.substring(2, 4);
      } else {
        hours = timestring.substring(0, 2);
        mins = timestring.substring(3, 5);
      }
      //let hours = timestring.substring(0, 1);
      //let mins = timestring.substring(-2, -3);

      if (hours !== "12") {
        if (hours === "11" || hours === "10") {
          hours = parseInt(hours.substring(0, 2));
          hours += 12;
        } else {
          hours = parseInt(hours.substring(0, 1));
          hours += 12;
        }
      }
      formattedTime = hours + ":" + mins;
      return formattedTime;
    }
  };

  const renderTimeSlots = (day, times) => {
    return (
      <FlatList
        data={times}
        renderItem={({ item }) => {
          return (
            <Card style={styles.card} key={day + item.id}>
              <View style={styles.internalCard}>
                <Text style={styles.cardText}>from</Text>
                <TouchableOpacity
                  onPress={() => showDatePicker(day, item.id, "start")}
                  style={styles.timeContainer}
                >
                  <Text style={styles.timeText}>
                    {availToString(item.start)}
                  </Text>
                </TouchableOpacity>
                <Text style={styles.cardText}>to</Text>
                <TouchableOpacity
                  onPress={() => showDatePicker(day, item.id, "end")}
                  style={styles.timeContainer}
                >
                  <Text style={styles.timeText}>{availToString(item.end)}</Text>
                </TouchableOpacity>
              </View>

              <AntDesign
                name="close"
                onPress={() => removeCard(day, item.id)}
                size={20}
                color="rgb(237, 75, 121)"
                style={styles.closeButton}
              />
            </Card>
          );
        }}
        keyExtractor={(item) => item.id}
        contentContainerStyle={{ padding: "1%" }}
        ItemSeparatorComponent={() => <View style={{ marginTop: "4%" }} />}
      />
    );
  };

  const renderTimeslotDirector = (dayString) => {
    switch (dayString) {
      case "Monday":
        return (
          <View key={"mondayCards"}>
            {renderTimeSlots("Monday", mondayTimes)}
          </View>
        );
      case "Tuesday":
        return (
          <View key={"tuesdayCards"}>
            {renderTimeSlots("Tuesday", tuesdayTimes)}
          </View>
        );
      case "Wednesday":
        return (
          <View key={"wednesdayCards"}>
            {renderTimeSlots("Wednesday", wednesdayTimes)}
          </View>
        );
      case "Thursday":
        return (
          <View key={"thursdayCards"}>
            {renderTimeSlots("Thursday", thursdayTimes)}
          </View>
        );
      case "Friday":
        return (
          <View key={"fridayCards"}>
            {renderTimeSlots("Friday", fridayTimes)}
          </View>
        );
      case "Saturday":
        return (
          <View key={"saturdayCards"}>
            {renderTimeSlots("Saturday", saturdayTimes)}
          </View>
        );
      case "Sunday":
        return (
          <View key={"sundayCards"}>
            {renderTimeSlots("Sunday", sundayTimes)}
          </View>
        );
    }
  };

  const listTabs = studentAvailability.map((data) => {
    let arrayLength = 0;
    switch (data.day) {
      case "Monday":
        arrayLength = mondayTimes.length;
        break;
      case "Tuesday":
        arrayLength = tuesdayTimes.length;
        break;
      case "Wednesday":
        arrayLength = wednesdayTimes.length;
        break;
      case "Thursday":
        arrayLength = thursdayTimes.length;
        break;
      case "Friday":
        arrayLength = fridayTimes.length;
        break;
      case "Saturday":
        arrayLength = saturdayTimes.length;
        break;
      case "Sunday":
        arrayLength = sundayTimes.length;
        break;
    }

    if (arrayLength === 0) {
      return (
        //RENDER EMPTY STATE TAB
        <ScrollView
          tabLabel={data.day.toUpperCase()}
          style={styles.emptyStateTabView}
          key={data.day + "tab"}
        >
          <View style={styles.emptyStateContainer}>
            <AntDesign
              name="pluscircle"
              size={55}
              color={Colors.secondaryColor}
              onPress={() => addElement(data.day)}
            />
            <Text style={styles.emptyPlusText}>ADD TO SCHEDULE</Text>
          </View>
        </ScrollView>
      );
    } else {
      return (
        //RENDER FULL STATE TAB
        <ScrollView
          tabLabel={data.day.toUpperCase()}
          style={styles.fullStateTabView}
          key={data.day + "tab"}
        >
          {renderTimeslotDirector(data.day)}
          <View>
            <TouchableOpacity style={styles.addToScheduleButton}>
              <AntDesign
                name="pluscircleo"
                size={50}
                color={Colors.secondaryColor}
                onPress={() => addElement(data.day)}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      );
    }
  });

  let defaultTime = new Date();
  defaultTime.setDate(defaultTime.getDate() - defaultTime.getDay());
  defaultTime.setHours(0);
  defaultTime.setMinutes(0);

  return (
    <SafeAreaView style={globalStyles.androidSafeArea}>
      {/*TAB BAR COMPONENT*/}
      <ScrollableTabView
        style={{ maxHeight: "87%" }}
        tabBarActiveTextColor={"rgb(237, 75, 121)"}
        tabBarUnderlineStyle={{
          backgroundColor: "rgb(237, 75, 121)",
          height: 2,
        }}
        initialPage={0}
        renderTabBar={() => <ScrollableTabBar />}
      >
        {listTabs}
      </ScrollableTabView>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="time"
        date={defaultTime}
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        key={"timePicker"}
        headerTextIOS="Choose a time"
        is24Hour
        locale="en_GB"
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: "row",
    flex: 1,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 25,
    position: "relative",
    marginStart: "10%",
  },
  emptyStateTabView: {
    flex: 1,
    minHeight: 550,
    flexDirection: "column",
    alignContent: "center",
  },
  fullStateTabView: {
    flex: 1,
    alignContent: "center",
  },
  cardText: {
    fontSize: 20,
    color: "rgba(0,0,0,0.5)",
    paddingEnd: "3%",
    paddingStart: "3%",
    flexWrap: "wrap",
    alignSelf: "center",
    textAlign: "center",
  },
  timeText: {
    fontSize: 30,
  },
  confirmText: {
    fontFamily: "raleway-medium",
    color: "rgb(237, 75, 121)",
    fontSize: 22,
    alignSelf: "flex-end",
  },
  addToScheduleButton: {
    alignSelf: "flex-end",
    alignItems: "center",
    justifyContent: "center",
    margin: "3%",
  },
  plusText: {
    fontFamily: "raleway",
    fontSize: 50,
    color: "rgb(237, 75, 121)",
    alignSelf: "center",
  },
  emptyPlusText: {
    marginTop: 10,
    fontSize: 20,
    color: "rgb(237, 75, 121)",
    alignSelf: "center",
  },
  emptyStateText: {
    fontFamily: "raleway",
    fontSize: 50,
    color: "white",
    textAlign: "center",
  },
  tabView: {
    flex: 1,
    padding: 10,
    backgroundColor: "rgb(237, 75, 121)",
  },
  card: {
    flex: 1,
    flexDirection: "row",
    width: "95%",
    borderWidth: 1,
    backgroundColor: "rgb(255,255,255)",
    borderColor: "rgba(0,0,0,0.1)",
    margin: "3%",
    marginBottom: 0,
    padding: 20,
  },
  timeContainer: {
    width: 95,
  },
  closeButton: {
    alignSelf: "center",
    textAlign: "center",
    position: "absolute",
    right: 10,
  },
  internalCard: {
    flexDirection: "row",
    alignContent: "center",
  },
  plusButton: {
    borderWidth: 1,
    borderColor: "rgb(0,0,5)",
    borderRadius: 50,
    width: 50,
    height: 50,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    padding: 0,
    marginTop: "3%",
  },
  emptyStateButton: {
    borderRadius: 65,
    width: 65,
    height: 65,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    // padding: 0,
    backgroundColor: "rgb(237, 75, 121)",
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 2, width: 2 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
  },
  emptyStateContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    marginTop: "50%",
  },
});

export default SetAvailabilityFunctional;
