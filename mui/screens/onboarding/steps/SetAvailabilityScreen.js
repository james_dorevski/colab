import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";

const SetAvailabilityScreen = () => {
  return (
    <View style={styles.screen}>
      <Image
        source={require("../../../../assets/colab.png")}
        style={styles.image}
        resizeMode="contain"
      />
      <Text style={styles.text}>
        We can find groups that work for your schedule. Tell us more about what
        time you would like to study each day.
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    marginTop: 90,
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
});

export default SetAvailabilityScreen;
