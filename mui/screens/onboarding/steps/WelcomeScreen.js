import React, { useEffect } from "react";
import { View, StyleSheet, Text, Image, SafeAreaView } from "react-native";
import globalStyles from "../../../styles/globalStyles";
import { API, Auth, graphqlOperation } from "aws-amplify";
import * as queries from "../../../../src/graphql/custom-queries";
import { defaultAvatars } from "../../../constants/DefaultAvatars";
import { useOnboardingContextState } from "../OnboardingContext";

const WelcomeScreen = () => {
  const onboardingState = useOnboardingContextState();

  const getUserDetails = async () => {
    try {
      const result = await API.graphql(
        graphqlOperation(queries.getStudent, {
          id: Auth.user.attributes.sub,
        })
      );

      return result.data.getStudent;
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getUserDetails().then((student) => {
      onboardingState.setAvatar(
        student.avatar ? { id: student.avatar } : defaultAvatars[0]
      );
      onboardingState.setCampus(student.campus ? student.campus : " ");
      onboardingState.setWam(student.wam ? student.wam : "");
      onboardingState.setSubjects(
        student.subjects
          ? student.subjects.items.map((item) => item.subject.code)
          : []
      );
      onboardingState.setExistingSubjectIds(
        student.subjects ? student.subjects.items.map((item) => item.id) : []
      );

      /*
      TODO add once availability can be passed back down from the backend and displayed
      on SetAvailabilityFunctional screen
       */
      // onboardingState.setAvailability(student.availabilities.items ? student.availabilities.items : []);
    });
  }, []);

  return (
    <SafeAreaView style={globalStyles.androidSafeArea}>
      <View style={styles.screen}>
        <Image
          source={require("../../../../assets/colab.png")}
          style={styles.image}
          resizeMode="contain"
        />
        <Text style={styles.text}>Woohoo, welcome to CoLAB!</Text>
        <Text style={styles.text}>
          We&apos;re here to help you get stellar grades by connecting you with
          your peers! Would you like to get set up now?
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
});

export default WelcomeScreen;
