import React from "react";
import {
  View,
  StyleSheet,
  Image,
  Text,
  ScrollView,
  SafeAreaView,
} from "react-native";
import ChipPicker from "../../../components/ChipPicker";
import { useOnboardingContextState } from "../OnboardingContext";
import globalStyles from "../../../styles/globalStyles";

const SetWamScreen = () => {
  const onboardingState = useOnboardingContextState();

  const wamArray = [
    { id: 1, name: "Pass" },
    { id: 2, name: "Credit" },
    { id: 3, name: "Distinction" },
    { id: 4, name: "High Distinction" },
  ];

  return (
    <SafeAreaView style={globalStyles.androidSafeArea}>
      <ScrollView>
        <View style={styles.screen}>
          <Image
            source={require("../../../../assets/colab.png")}
            style={styles.image}
            resizeMode="contain"
          />
          <Text style={styles.text}>
            Knowing your WAM helps us form well-rounded study groups, so nobody
            gets left behind.
          </Text>
          <Text style={styles.text}>What&apos;s yours?</Text>

          <ChipPicker
            items={wamArray}
            value={onboardingState.wam}
            onValueSelect={(selectedWam) => {
              onboardingState.setWam(selectedWam.name);
            }}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
});

export default SetWamScreen;
