import React from "react";
import { Button } from "react-native-paper";
import { Avatar } from "react-native-elements";
import Colors from "../../../constants/Colors";
import {
  Modal,
  TouchableHighlight,
  StyleSheet,
  Text,
  Image,
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import AvatarPicker from "../../../components/AvatarPicker";
import { useOnboardingContextState } from "../OnboardingContext";
import globalStyles from "../../../styles/globalStyles";
import { defaultAvatars } from "../../../constants/DefaultAvatars";

const SetAvatar = () => {
  const onboardingState = useOnboardingContextState();
  const [selectedAvatar, setSelectedAvatar] = React.useState(
    onboardingState.avatar
  );
  const [modalVisible, setModalVisible] = React.useState(false);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  return (
    <SafeAreaView style={globalStyles.androidSafeArea}>
      <View style={styles.screen}>
        <Image
          source={require("../../../../assets/colab.png")}
          style={styles.image}
          resizeMode="contain"
        />
        <Text style={styles.text}>Set your profile picture.</Text>
        <TouchableOpacity onPress={openModal}>
          <View style={styles.avatarContainer}>
            <Avatar
              rounded
              source={{ uri: defaultAvatars[selectedAvatar.id].uri }}
              size={220}
            />
          </View>
        </TouchableOpacity>

        <View style={styles.chooseAvatarButtonContainer}>
          <Button mode="text" color={Colors.secondaryColor} onPress={openModal}>
            <Text style={styles.buttonText}>CHOOSE ANOTHER AVATAR</Text>
          </Button>
          <View styles={styles.modalContainer}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={closeModal}
            >
              <ScrollView style={styles.modalContainer}>
                <View style={styles.modalView}>
                  <AvatarPicker
                    value={onboardingState.avatar.id}
                    onValueSelect={(selectedAvatar) => {
                      setSelectedAvatar(selectedAvatar);
                      onboardingState.setAvatar(selectedAvatar);
                    }}
                  />

                  {/*Close Modal button*/}
                  <View style={styles.buttonContainer}>
                    <TouchableHighlight
                      style={{
                        ...styles.openButton,
                        backgroundColor: Colors.primaryColor,
                      }}
                      onPress={closeModal}
                    >
                      <Text style={styles.textStyle}>OK</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </ScrollView>
            </Modal>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
  avatarContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 100,
    padding: 10,
  },
  chooseAvatarButtonContainer: {
    marginTop: 120,
  },
  modalContainer: {
    flex: 1,
    alignSelf: "center",
    marginTop: 22,
    width: "98%",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 5,
    paddingHorizontal: 20,
    paddingVertical: 10,
    elevation: 2,
  },
  buttonContainer: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  textStyle: {
    fontSize: 18,
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});

export default SetAvatar;
