import React, { useState } from "react";
import { defaultAvatars } from "../../constants/DefaultAvatars";

export const OnboardingContext = React.createContext();

export const OnboardingProvider = ({ children }) => {
  const [isComplete, setIsComplete] = useState(false);
  const [isSubmitLoading, setIsSubmitLoading] = useState(false);
  const [isSubmissionReceived, setIsSubmissionReceived] = useState(false);
  const [avatar, setAvatar] = useState(defaultAvatars[0]);
  const [campus, setCampus] = useState({});
  const [wam, setWam] = useState("");
  const [subjects, setSubjects] = useState([]);
  const [availability, setAvailability] = useState([]);
  const [existingSubjectIds, setExistingSubjectIds] = useState([]);

  const onboardingState = {
    isSubmitLoading,
    isSubmissionReceived,
    isComplete,
    avatar,
    campus,
    wam,
    subjects,
    availability,
    existingSubjectIds,
    setIsSubmitLoading,
    setIsSubmissionReceived,
    setIsComplete,
    setAvatar,
    setCampus,
    setWam,
    setSubjects,
    setAvailability,
    setExistingSubjectIds,
  };

  return (
    <OnboardingContext.Provider value={onboardingState}>
      {children}
    </OnboardingContext.Provider>
  );
};

export const useOnboardingContextState = () => {
  const context = React.useContext(OnboardingContext);

  if (context === undefined) {
    throw new Error(
      "useOnboardingContextState must be within a OnboardingProvider"
    );
  }

  return context;
};
