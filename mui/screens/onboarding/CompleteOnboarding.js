import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Text } from "react-native-paper";
import GradientButton from "../../components/GradientButton";
import Colors from "../../constants/Colors";
import { Auth } from "aws-amplify";

const CompleteOnboarding = (props) => {
  const signOut = async () => {
    try {
      await Auth.signOut();
    } catch (err) {
      console.error("error signing out: ", err);
    }
  };

  return (
    <View style={styles.screen}>
      <Image
        source={require("../../../assets/colab.png")}
        style={styles.image}
        resizeMode="contain"
      />
      <Text style={styles.text}>
        Thanks for that! We&apos;re now ready to set you up with some study
        partners!
      </Text>

      <View>
        <View style={styles.buttonContainer}>
          <GradientButton
            style={styles.button}
            label="Find my groups"
            gradientBegin={Colors.primaryColor}
            gradientEnd={Colors.secondaryColor}
            disabledGradientBegin={Colors.primaryColorDisabled}
            disabledGradientEnd={Colors.secondaryColorDisabled}
            gradientDirection="vertical"
            impact
            impactStyle="Light"
            onPressAction={props.onSubmit}
          />
        </View>
      </View>
      <View style={styles.textContainer}>
        <Text
          onPress={signOut}
          style={{
            fontFamily: "raleway-medium",
            fontSize: 16,
            color: Colors.secondaryColor,
          }}
        >
          Sign out
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
    marginTop: 90,
    padding: 30,
  },
  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  text: {
    fontFamily: "raleway",
    fontSize: 30,
    marginBottom: 35,
  },
  textContainer: {
    marginTop: "5%",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default CompleteOnboarding;
