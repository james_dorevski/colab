/* eslint-disable react/jsx-key */
import React, { useState } from "react";
import WelcomeScreen from "./steps/WelcomeScreen";
import SetCampusScreen from "./steps/SetCampusScreen";
import SetWamScreen from "./steps/SetWamScreen";
import SetSubjectsScreen from "./steps/SetSubjectsScreen";
import CompleteOnboarding from "./CompleteOnboarding";
import { Alert, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import Colors from "../../constants/Colors";
import SetAvatar from "./steps/SetAvatar";
import { useOnboardingContextState } from "./OnboardingContext";
import onboardingStyles from "../../styles/onboardStyles";
import { Auth, API, graphqlOperation } from "aws-amplify";
import * as mutations from "../../../src/graphql/custom-mutations";
import Loading from "../../components/Loading";
import SetAvailabilityScreen from "./steps/SetAvailabilityScreen";
import SetAvailabilityFunctional from "./steps/SetAvailabilityFunctional";
import globalStyles from "../../styles/globalStyles";

const useFormProgress = () => {
  const [currentStep, setCurrentStep] = useState(0);

  function nextStep() {
    setCurrentStep(currentStep + 1);
  }

  function previousStep() {
    setCurrentStep(currentStep - 1);
  }

  function returnToStart() {
    setCurrentStep(0);
  }

  return [currentStep, nextStep, previousStep, returnToStart];
};

const OnboardingForm = () => {
  const onboardingState = useOnboardingContextState();

  async function updateUserInformation() {
    try {
      await API.graphql(
        graphqlOperation(mutations.updateStudent, {
          input: {
            id: Auth.user.attributes.sub,
            studentUniversityId: 1, // set to UOW because that's the only uni we're supporting atm
            avatar: onboardingState.avatar.id,
            studentCampusId: onboardingState.campus.id,
            wam: onboardingState.wam,
          },
        })
      );

      /*
       * TODO handle replacing existing availabilities instead of adding a thousand new ones, too
       */
      await Promise.all(
        onboardingState.existingSubjectIds.map((id) => {
          API.graphql(
            graphqlOperation(mutations.deleteStudentSubject, {
              input: {
                id: id,
              },
            })
          );
        })
      );

      onboardingState.setExistingSubjectIds([]);

      await Promise.all(
        onboardingState.subjects.map((subject) => {
          return API.graphql(
            graphqlOperation(mutations.createStudentSubject, {
              input: {
                studentSubjectStudentId: Auth.user.attributes.sub,
                studentSubjectSubjectId: subject,
              },
            })
          ).then((result) => {
            onboardingState.setExistingSubjectIds([
              ...onboardingState.existingSubjectIds,
              result.data.createStudentSubject.id,
            ]);
          });
        })
      );

      return Promise.all(
        onboardingState.availability.map((availability) => {
          API.graphql(
            graphqlOperation(mutations.createAvailability, {
              input: {
                availabilityStudentId: Auth.user.attributes.sub,
                day: availability.name,
                startTime: availability.start,
                endTime: availability.end,
              },
            })
          );
        })
      );
    } catch (err) {
      Alert.alert("Error Updating User Profile", err.message, [{ text: "Ok" }]);
    }
  }

  const handleSubmit = () => {
    onboardingState.setIsSubmitLoading(true);
    // waiting until backend information is updated
    updateUserInformation().then(() => {
      onboardingState.setIsSubmitLoading(false);
      onboardingState.setIsSubmissionReceived(true);
      returnToStart();
    });
  };

  const steps = [
    <WelcomeScreen />,
    <SetAvatar />,
    <SetCampusScreen />,
    <SetWamScreen />,
    <SetAvailabilityScreen />,
    <SetAvailabilityFunctional />,
    <SetSubjectsScreen />,
    <CompleteOnboarding onSubmit={handleSubmit} />,
  ];

  const [
    currentStep,
    nextStep,
    previousStep,
    returnToStart,
  ] = useFormProgress();
  const isFirst = currentStep === 0;
  const isLast = currentStep === steps.length - 1;

  if (onboardingState.isSubmitLoading) {
    return <Loading />;
  }

  if (isLast) {
    return (
      <SafeAreaView style={globalStyles.androidSafeArea}>
        {steps[currentStep]}
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={globalStyles.androidSafeArea}>
        {steps[currentStep]}
        <View style={styles.bottomButtonContainer}>
          {/*temporary back button*/}
          {!isFirst && (
            <Button
              color={Colors.backButton}
              style={onboardingStyles.secondaryButton}
              onPress={() => {
                previousStep();
              }}
            >
              Back
            </Button>
          )}
          {!isLast && (
            <>
              <Button
                mode="text"
                color={Colors.secondaryColor}
                style={onboardingStyles.secondaryButton}
                onPress={() => handleSubmit()} /*go to dashboard*/
              >
                <Text style={styles.laterButtonText}>GO TO DASH</Text>
              </Button>
              <Button
                mode="contained"
                type="submit"
                color={Colors.secondaryColor}
                style={onboardingStyles.primaryButton}
                onPress={() => {
                  nextStep();
                }}
              >
                <Text style={styles.nextButtonText}>
                  {isLast ? "SAVE" : "NEXT"}
                </Text>
              </Button>
            </>
          )}
        </View>
      </SafeAreaView>
    );
  }
};

const styles = StyleSheet.create({
  bottomButtonContainer: {
    flexDirection: "row",
    position: "absolute",
    bottom: "5%",
    right: "5%",
  },
  nextButtonText: {
    fontFamily: "raleway-bold",
  },
  laterButtonText: {
    fontFamily: "raleway",
  },
});

export default OnboardingForm;
