import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Auth } from "aws-amplify";
import { Button } from "react-native-paper";
import { MaterialIcons } from "@expo/vector-icons";
import { Avatar } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import PropTypes from "prop-types";
import { useUserContextState } from "../../UserContext";
import { defaultAvatars } from "../../constants/DefaultAvatars";
import { handleSignOut } from "../../services/UserInformationService";

const SettingsMainScreen = ({ navigation }) => {
  const user = useUserContextState();

  return (
    <ScrollView style={styles.screen}>
      <TouchableOpacity
        style={styles.loggedInUser}
        onPress={() =>
          navigation.navigate("UserProfile", {
            userId: Auth.user.attributes.sub,
          })
        }
      >
        <View style={{ backgroundColor: "white", padding: 10 }}>
          <Avatar
            size={65}
            source={{ uri: defaultAvatars[user.avatar].uri }}
            rounded
          />
        </View>
        <View style={styles.userInfoText}>
          <Text style={styles.username}>{user.name}</Text>
          <Text style={styles.campusDetails}>{user.university}</Text>
          <Text style={styles.viewProfileString}>View Profile</Text>
        </View>
        <MaterialIcons name={"keyboard-arrow-right"} size={30} />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.settingsLinkContainer}
        onPress={() => navigation.navigate("AccountSettings")}
      >
        <Text style={styles.settingsLinkString}>Account</Text>
        <MaterialIcons
          name={"keyboard-arrow-right"}
          size={30}
          style={{ marginLeft: "15%" }}
        />
      </TouchableOpacity>

      <View style={styles.buttonContainer}>
        <Button
          style={styles.logOutButton}
          mode="contained"
          color="black"
          onPress={() => handleSignOut()}
        >
          <Text style={{ fontFamily: "raleway-bold", fontSize: 14 }}>
            LOG OUT
          </Text>
        </Button>
      </View>
    </ScrollView>
  );
};

SettingsMainScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  loggedInUser: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: "8%",
    marginHorizontal: "5%",
  },
  userInfoText: {},
  username: {
    fontFamily: "raleway-bold",
    fontSize: 22,
  },
  settingsLinkContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: "8%",
    marginLeft: "7%",
    marginRight: "5%",
    justifyContent: "space-between",
  },
  settingsLinkString: {
    fontFamily: "raleway",
    fontSize: 20,
  },
  buttonContainer: {
    marginTop: "10%",
    alignItems: "center",
  },
  logOutButton: {
    width: 125,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default SettingsMainScreen;
