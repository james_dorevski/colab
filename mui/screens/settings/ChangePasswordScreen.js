import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { Formik } from "formik";
import * as yup from "yup";
import { updatePassword } from "../../services/UserInformationService";
import Colors from "../../constants/Colors";

const ChangePasswordScreen = ({ navigation }) => {
  return (
    <View style={styles.screen}>
      <Formik
        initialValues={{
          oldPassword: "",
          newPassword: "",
          confirmNewPassword: "",
        }}
        onSubmit={(values) => updatePassword(values).then(navigation.goBack())}
        validationSchema={yup.object().shape({
          oldPassword: yup.string().min(8).required("Old password is required"),
          newPassword: yup.string().min(8).required("New password is required"),
          confirmNewPassword: yup
            .string()
            .oneOf([yup.ref("newPassword"), null], "New passwords must match")
            .required(),
        })}
      >
        {({
          values,
          handleChange,
          errors,
          handleSubmit,
          touched,
          setFieldTouched,
          dirty,
          isValid,
        }) => (
          <>
            <View style={styles.inputContainer}>
              <View style={styles.textInput}>
                <TextInput
                  value={values.oldPassword}
                  onChangeText={handleChange("oldPassword")}
                  label="Old Password"
                  secureTextEntry={true}
                  onBlur={() => setFieldTouched("oldPassword")}
                  mode="outlined"
                  theme={{ colors: { primary: Colors.primaryColor } }}
                />
              </View>
              {touched.oldPassword && errors.oldPassword && (
                <Text style={styles.errorText}>{errors.oldPassword}</Text>
              )}

              <View style={styles.textInput}>
                <TextInput
                  value={values.newPassword}
                  onChangeText={handleChange("newPassword")}
                  label="New Password"
                  secureTextEntry={true}
                  onBlur={() => setFieldTouched("newPassword")}
                  mode="outlined"
                  theme={{ colors: { primary: Colors.primaryColor } }}
                />
              </View>
              {touched.newPassword && errors.newPassword && (
                <Text style={styles.errorText}>{errors.newPassword}</Text>
              )}

              <View style={styles.textInput}>
                <TextInput
                  value={values.confirmNewPassword}
                  onChangeText={handleChange("confirmNewPassword")}
                  label="Confirm New Password"
                  secureTextEntry={true}
                  onBlur={() => setFieldTouched("confirmNewPassword")}
                  mode="outlined"
                  theme={{ colors: { primary: Colors.primaryColor } }}
                />
              </View>
              {touched.confirmNewPassword && errors.confirmNewPassword && (
                <Text style={styles.errorText}>
                  {errors.confirmNewPassword}
                </Text>
              )}

              <View style={{ alignItems: "center", marginTop: "5%" }}>
                <Button
                  mode="contained"
                  disabled={!(isValid && dirty)}
                  onPress={handleSubmit}
                  style={styles.changePasswordButton}
                  color={Colors.secondaryColor}
                >
                  Change
                </Button>
              </View>
            </View>
          </>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  inputContainer: {
    marginTop: "5%",
    marginHorizontal: "8%",
  },
  errorText: {
    color: Colors.errorText,
  },
  changePasswordButton: {
    width: 120,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default ChangePasswordScreen;
