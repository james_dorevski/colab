import React from "react";
import { View, StyleSheet, Alert } from "react-native";
import { Button } from "react-native-paper";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import Colors from "../../constants/Colors";
import { MaterialIcons } from "@expo/vector-icons";
import { useUserContextState } from "../../UserContext";
import { API, Auth, graphqlOperation } from "aws-amplify";
import * as queries from "../../../src/graphql/custom-queries";
import * as mutations from "../../../src/graphql/custom-mutations";
import Loading from "../../components/Loading";
import { SafeAreaView } from "react-native-safe-area-context";

const UpdateSubjectScreen = () => {
  const user = useUserContextState();

  const [loading, setLoading] = React.useState(true);
  const [items, setItems] = React.useState([
    {
      name: "See All",
      id: 0,
      children: [],
    },
  ]);

  React.useEffect(() => {
    setLoading(true);
    getAllSubjects();
  }, []);

  const getAllSubjects = async () => {
    setLoading(true);
    try {
      await API.graphql(graphqlOperation(queries.listSubjects)).then(
        (result) => {
          setItems([
            {
              name: "See All",
              id: 0,
              children: result.data.listSubjects.items.map((subject) => {
                return {
                  name: `${subject.code} ${subject.name}`,
                  id: subject.id,
                };
              }),
            },
          ]);
        }
      );
      setLoading(false);
    } catch (err) {
      Alert.alert("Error Retrieving Subjects", err.message, [{ text: "Ok" }]);
    }
  };

  const handleSubmit = async () => {
    setLoading(true);
    try {
      await Promise.all(
        user.existingSubjectIds.map((id) => {
          API.graphql(
            graphqlOperation(mutations.deleteStudentSubject, {
              input: {
                id: id,
              },
            })
          );
        })
      );

      user.setExistingSubjectIds([]);

      await Promise.all(
        user.subjectCodes.map((subject) => {
          return API.graphql(
            graphqlOperation(mutations.createStudentSubject, {
              input: {
                studentSubjectStudentId: Auth.user.attributes.sub,
                studentSubjectSubjectId: subject,
              },
            })
          ).then((result) => {
            user.setExistingSubjectIds([
              ...user.existingSubjectIds,
              result.data.createStudentSubject.id,
            ]);
          });
        })
      );

      await API.graphql(
        graphqlOperation(queries.getStudent, {
          id: Auth.user.attributes.sub,
        })
      ).then((result) =>
        user.setSubjects(result.data.getStudent.subjects.items)
      );

      setLoading(false);
    } catch (err) {
      Alert.alert("Error Updating User Subjects", err.message, [
        { text: "Ok" },
      ]);
    }
  };

  return loading ? (
    <Loading />
  ) : (
    <SafeAreaView style={styles.screen}>
      <View style={{ marginHorizontal: "5%" }}>
        <SectionedMultiSelect
          items={items}
          uniqueKey="id"
          subKey="children"
          selectText="Select your subjects..."
          showDropDowns={true}
          readOnlyHeadings={true}
          selectedItems={[...new Set(user.subjectCodes)]}
          onSelectedItemsChange={(selectedSubjects) => {
            user.setSubjectCodes([...selectedSubjects]);
          }}
          styles={{
            button: { backgroundColor: Colors.primaryColor },
            container: { marginVertical: "35%" },
          }}
          IconRenderer={MaterialIcons}
          loading={loading}
          selectLabelNumberOfLines={1}
          itemNumberOfLines={1}
        />

        <View style={{ alignItems: "center", marginTop: "5%" }}>
          <Button
            mode="contained"
            onPress={handleSubmit}
            style={styles.changePasswordButton}
            color={Colors.secondaryColor}
          >
            Update
          </Button>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  changePasswordButton: {
    width: 120,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default UpdateSubjectScreen;
