import React, { useState, useEffect } from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { Portal, Dialog, Button, RadioButton } from "react-native-paper";
import { ScrollView } from "react-native-gesture-handler";
import { useUserContextState } from "../../UserContext";
import Colors from "../../constants/Colors";
import {
  updateWam,
  updateCampus,
  updatePreferredLanguage,
} from "../../services/UserInformationService";

const AccountSettingsScreen = ({ navigation }) => {
  const user = useUserContextState();

  const [wamDialogVisible, setWamDialogVisible] = useState(false);
  const [wamDialogValue, setWamDialogValue] = useState();
  const showUpdateWamDialog = () => setWamDialogVisible(true);
  const hideUpdateWamDialog = () => setWamDialogVisible(false);

  const [prefLangDialogVisible, setPrefLangDialogVisible] = useState(false);
  const [prefLangDialogValue, setPrefLangDialogValue] = useState();
  const showPrefLangDialog = () => setPrefLangDialogVisible(true);
  const hidePrefLangDialog = () => setPrefLangDialogVisible(false);

  const [campusDialogVisible, setCampusDialogVisible] = useState(false);
  const [campusDialogValue, setCampusDialogValue] = useState();
  const showCampusDialog = () => setCampusDialogVisible(true);
  const hideCampusDialog = () => setCampusDialogVisible(false);

  useEffect(() => {}, [user.wam, user.preferredLanguage, user.campus]);

  return (
    <ScrollView style={styles.screen}>
      <Portal>
        <Dialog visible={wamDialogVisible} onDismiss={hideUpdateWamDialog}>
          <Dialog.Title>Change WAM</Dialog.Title>
          <RadioButton.Group
            onValueChange={(wamDialogValue) =>
              setWamDialogValue(wamDialogValue)
            }
            value={wamDialogValue}
          >
            <RadioButton.Item label="Pass" value="Pass" />
            <RadioButton.Item label="Credit" value="Credit" />
            <RadioButton.Item label="Distinction" value="Distinction" />
            <RadioButton.Item
              label="High Distinction"
              value="High Distinction"
            />
          </RadioButton.Group>
          <Dialog.Actions>
            <Button onPress={hideUpdateWamDialog} color={Colors.secondaryColor}>
              Cancel
            </Button>
            <Button
              onPress={() => {
                updateWam(user.id, wamDialogValue).then(
                  user.setWam(wamDialogValue)
                );
                hideUpdateWamDialog();
              }}
              color={Colors.secondaryColor}
            >
              Ok
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      <Portal>
        <Dialog visible={prefLangDialogVisible} onDismiss={hidePrefLangDialog}>
          <Dialog.Title>Change Preferred Language</Dialog.Title>
          <RadioButton.Group
            onValueChange={(prefLangDialogValue) =>
              setPrefLangDialogValue(prefLangDialogValue)
            }
            value={prefLangDialogValue}
          >
            <RadioButton.Item label="English" value="English" />
          </RadioButton.Group>
          <Dialog.Actions>
            <Button onPress={hidePrefLangDialog} color={Colors.secondaryColor}>
              Cancel
            </Button>
            <Button
              onPress={() => {
                updatePreferredLanguage(user.id, prefLangDialogValue).then(
                  user.setPreferredLanguage(prefLangDialogValue)
                );
                hidePrefLangDialog();
              }}
              color={Colors.secondaryColor}
            >
              Ok
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      <Portal>
        <Dialog visible={campusDialogVisible} onDismiss={hideCampusDialog}>
          <Dialog.Title>Change Campus</Dialog.Title>
          <RadioButton.Group
            onValueChange={(campusDialogValue) =>
              setCampusDialogValue(campusDialogValue)
            }
            value={campusDialogValue}
          >
            <RadioButton.Item label="Wollongong" value="1" />
            <RadioButton.Item label="SWS" value="2" />
            <RadioButton.Item label="Shoalhaven" value="3" />
            <RadioButton.Item label="Innovation" value="4" />
          </RadioButton.Group>
          <Dialog.Actions>
            <Button onPress={hideCampusDialog} color={Colors.secondaryColor}>
              Cancel
            </Button>
            <Button
              onPress={() => {
                updateCampus(user.id, campusDialogValue).then((results) =>
                  user.setCampus(results)
                );
                hideCampusDialog();
              }}
              color={Colors.secondaryColor}
            >
              Ok
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>

      <TouchableOpacity
        style={styles.setting}
        onPress={() => navigation.navigate("ChangePassword")}
      >
        <Text style={styles.settingsLinkString}>Password</Text>
        <Text style={styles.currentValueString}>**********</Text>
        <MaterialIcons name={"keyboard-arrow-right"} size={28} />
      </TouchableOpacity>

      {/* TODO: change subject */}
      <TouchableOpacity
        style={styles.setting}
        onPress={() => navigation.navigate("UpdateSubjects")}
      >
        <Text style={styles.settingsLinkString}>Subjects</Text>
        <Text style={styles.currentValueString}></Text>
        <MaterialIcons name={"keyboard-arrow-right"} size={28} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.setting} onPress={showUpdateWamDialog}>
        <Text style={styles.settingsLinkString}>WAM</Text>
        <Text style={styles.currentValueString}>{user.wam}</Text>
        <MaterialIcons name={"keyboard-arrow-right"} size={28} />
      </TouchableOpacity>

      {/* TODO: change schedule */}
      <TouchableOpacity style={styles.setting}>
        <Text style={styles.settingsLinkString}>Schedule</Text>
        <Text style={styles.currentValueString}></Text>
        <MaterialIcons name={"keyboard-arrow-right"} size={28} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.setting} onPress={showPrefLangDialog}>
        <Text style={styles.settingsLinkString}>Preferred Language</Text>
        <Text style={styles.currentValueString}>{user.preferredLanguage}</Text>
        <MaterialIcons name={"keyboard-arrow-right"} size={28} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.setting} onPress={showCampusDialog}>
        <Text style={styles.settingsLinkString}>Campus</Text>
        <Text style={styles.currentValueString}>{user.campus}</Text>
        <MaterialIcons name={"keyboard-arrow-right"} size={28} />
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  setting: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: "3%",
    marginLeft: "8%",
    marginRight: "6%",
  },
  settingsLinkString: {
    fontFamily: "raleway-bold",
    fontSize: 20,
  },
  currentValueString: {
    fontFamily: "raleway",
    fontSize: 20,
    position: "absolute",
    textAlign: "right",
    end: "10%",
  },
});

export default AccountSettingsScreen;
