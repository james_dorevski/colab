import React from "react";
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  Alert,
  Image,
} from "react-native";
import { TextInput, Button } from "react-native-paper";
import { Formik } from "formik";
import * as yup from "yup";
import { Auth } from "aws-amplify";
import Colors from "../../constants/Colors";
import authenticationStyles from "../../styles/authenticationStyles";
import globalStyles from "../../styles/globalStyles";

const ForgotPasswordScreen = (props) => {
  async function onSubmit(values) {
    try {
      await Auth.forgotPassword(values.email);
      props.onStateChange("requireNewPassword", {});
    } catch (err) {
      Alert.alert("Error Forgetting Password", err.message, [{ text: "Ok" }]);
    }
  }

  if (props.authState === "forgotPassword") {
    return (
      <KeyboardAvoidingView style={authenticationStyles.screen}>
        <Image
          source={require("../../../assets/colab.png")}
          style={authenticationStyles.image}
          resizeMode="contain"
        />

        <Text style={authenticationStyles.bigText}>Forgotten Password?</Text>
        <Text style={authenticationStyles.littleText}>
          No dramas! Enter your email address and we'll send you a code to have
          it reset.
        </Text>

        <View>
          <Formik
            initialValues={{
              email: "",
            }}
            onSubmit={(values) => onSubmit(values)}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email("Not a valid email address")
                .required("E-mail is required"),
            })}
          >
            {({
              values,
              handleChange,
              errors,
              handleSubmit,
              touched,
              setFieldTouched,
              dirty,
              isValid,
            }) => (
              <>
                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.email}
                    onChangeText={handleChange("email")}
                    label="E-mail"
                    autoCapitalize="none"
                    autoCompleteType="email"
                    onBlur={() => setFieldTouched("email")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>
                {touched.email && errors.email && (
                  <Text style={globalStyles.errorText}>{errors.email}</Text>
                )}

                <View style={{flexDirection: "row-reverse"}}>
                  <View style={authenticationStyles.buttonContainer}>
                    <Button
                      mode="contained"
                      disabled={!(isValid && dirty)}
                      onPress={handleSubmit}
                      style={authenticationStyles.primaryButton}
                      color={Colors.secondaryColor}
                    >
                      Send Code
                    </Button>
                  </View>

                  <View style={authenticationStyles.buttonContainer}>
                    <Button
                      mode="text"
                      onPress={() => props.onStateChange("signIn", [])}
                      style={authenticationStyles.secondaryButton}
                      color={Colors.secondaryColor}
                    >
                      Cancel
                    </Button>
                  </View>
                </View>
              </>
            )}
          </Formik>
        </View>
      </KeyboardAvoidingView>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  textInput: {
    marginTop: "5%",
  },
});

export default ForgotPasswordScreen;
