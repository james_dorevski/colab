import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Alert,
  Platform,
  KeyboardAvoidingView,
} from "react-native";
import { TextInput, Button } from "react-native-paper";
import { Formik } from "formik";
import * as yup from "yup";
import { Auth } from "aws-amplify";
import Colors from "../../constants/Colors";

const ConfirmSignUpScreen = (props) => {
  async function onSubmit(values) {
    try {
      await Auth.confirmSignUp(values.email, values.code);

      props.onStateChange("signIn", {});
    } catch (err) {
      Alert.alert("Error Confirming Account", err.message, [{ text: "Ok" }]);
    }
  }

  if (props.authState === "confirmSignUp") {
    return (
      <KeyboardAvoidingView style={styles.screen} behavior={"position"}>
        <Image
          source={require("../../../assets/colab.png")}
          style={styles.image}
          resizeMode="contain"
        />

        <Text style={styles.bigText}>Thanks for signing up!</Text>
        <Text style={styles.littleText}>
          We sent you an eight-digit verification code to your email address.
          Enter it with your email below:
        </Text>

        <View style={styles.inputContainer}>
          <Formik
            initialValues={{
              email: "",
              code: "",
            }}
            onSubmit={(values) => onSubmit(values)}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email("Not a valid email address")
                .required("E-mail is required"),
              code: yup.number().required("Verification code is required"),
            })}
          >
            {({
              values,
              handleChange,
              errors,
              handleSubmit,
              touched,
              setFieldTouched,
              isValid,
            }) => (
              <View style={{}}>
                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.email}
                    onChangeText={handleChange("email")}
                    placeholder="E-mail"
                    onBlur={() => setFieldTouched("email")}
                    autoCompleteType="email"
                    autoCapitalize="none"
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>
                {touched.email && errors.email && (
                  <Text style={styles.errorText}>{errors.email}</Text>
                )}
                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.code}
                    onChangeText={handleChange("code")}
                    keyboardType="number-pad"
                    placeholder="Verification Code"
                    onBlur={() => setFieldTouched("code")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>
                {touched.code && errors.code && (
                  <Text style={styles.errorText}>{errors.code}</Text>
                )}
                <View style={styles.bottomButtonContainer}>
                  <Button
                    onPress={() => props.onStateChange("signUp", {})}
                    color={Colors.secondaryColor}
                  >
                    GO BACK
                  </Button>
                  <Button
                    mode="contained"
                    onPress={handleSubmit}
                    style={styles.button}
                    color={Colors.secondaryColor}
                    disabled={!isValid}
                  >
                    CONFIRM
                  </Button>
                </View>
              </View>
            )}
          </Formik>
        </View>
      </KeyboardAvoidingView>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  screen: {
    padding: 30,
  },
  image: {
    width: 110,
    height: 120,
  },
  bigText: {
    fontFamily: "raleway",
    fontSize: 30,
    marginTop: 5,
  },
  littleText: {
    fontFamily: "raleway",
    fontSize: 20,
    marginTop: 5,
  },
  inputContainer: {
    marginTop: 15,
  },
  textInput: {
    marginTop: 10,
  },
  errorText: {
    color: Colors.errorText,
  },
  bottomButtonContainer: {
    flexDirection: "row",
    marginTop: 20,
    alignSelf: "flex-end",
  },
});

export default ConfirmSignUpScreen;
