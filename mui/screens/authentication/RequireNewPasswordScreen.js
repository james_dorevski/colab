import React from "react";
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  Image,
  Alert,
} from "react-native";
import { TextInput, Button } from "react-native-paper";
import { Formik } from "formik";
import * as yup from "yup";
import { Auth } from "aws-amplify";
import Colors from "../../constants/Colors";
import authenticationStyles from "../../styles/authenticationStyles";

const RequireNewPasswordScreen = (props) => {
  async function onSubmit(values) {
    Auth.forgotPasswordSubmit(values.email, values.code, values.newPassword)
      .then(() => {
        Alert.alert("Alert", "Password successfully changed!", [
          { text: "Ok" },
        ]);
        props.onStateChange("signIn", []);
      })
      .catch((err) =>
        Alert.alert("Error Updating Password", err.message, [{ text: "Ok" }])
      );
  }

  if (props.authState === "requireNewPassword") {
    return (
      <KeyboardAvoidingView style={authenticationStyles.screen}>
        <Image
          source={require("../../../assets/colab.png")}
          style={authenticationStyles.image}
          resizeMode="contain"
        />

        <Text style={authenticationStyles.bigText}>Enter New Password</Text>
        <Text style={authenticationStyles.littleText}>
          Enter your email along with the code you received and your new
          password then you're good to go!
        </Text>

        <View style={{ marginVertical: "5%" }}>
          <Formik
            initialValues={{
              email: "",
              code: "",
              newPassword: "",
            }}
            onSubmit={(values) => onSubmit(values)}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email("Not a valid email address")
                .required("E-mail is required"),
              code: yup.number().required("Verification code is required"),
              newPassword: yup
                .string()
                .min(8)
                .required("New password is required"),
            })}
          >
            {({
              values,
              handleChange,
              errors,
              handleSubmit,
              touched,
              setFieldTouched,
              dirty,
              isValid,
            }) => (
              <>
                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.email}
                    onChangeText={handleChange("email")}
                    label="E-mail"
                    autoCapitalize="none"
                    autoCompleteType="email"
                    onBlur={() => setFieldTouched("email")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>
                {touched.email && errors.email && (
                  <Text style={authenticationStyles.errorText}>
                    {errors.email}
                  </Text>
                )}

                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.code}
                    onChangeText={handleChange("code")}
                    label="Code"
                    keyboardType="number-pad"
                    onBlur={() => setFieldTouched("code")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>
                {touched.code && errors.code && (
                  <Text style={styles.errorText}>{errors.code}</Text>
                )}

                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.newPassword}
                    onChangeText={handleChange("newPassword")}
                    label="New Password"
                    secureTextEntry={true}
                    onBlur={() => setFieldTouched("newPassword")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>
                {touched.newPassword && errors.newPassword && (
                  <Text style={styles.errorText}>{errors.newPassword}</Text>
                )}

                <View style={{flexDirection: "row-reverse"}}>
                  <View style={authenticationStyles.buttonContainer}>
                    <Button
                      mode="contained"
                      disabled={!(isValid && dirty)}
                      onPress={handleSubmit}
                      style={authenticationStyles.primaryButton}
                      color={Colors.secondaryColor}
                    >
                      Update Password
                    </Button>
                  </View>

                  <View style={authenticationStyles.buttonContainer}>
                    <Button
                      mode="text"
                      onPress={() => props.onStateChange("signIn", [])}
                      style={authenticationStyles.secondaryButton}
                      color={Colors.secondaryColor}
                    >
                      Cancel
                    </Button>
                  </View>
                </View>
              </>
            )}
          </Formik>
        </View>
      </KeyboardAvoidingView>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  textInput: {
    marginVertical: "1%",
  },
});

export default RequireNewPasswordScreen;
