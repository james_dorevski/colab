import React from "react";
import { View, StyleSheet, Text, Image, Alert, ScrollView } from "react-native";
import { TextInput } from "react-native-paper";
import { Formik } from "formik";
import * as yup from "yup";
import { Auth } from "aws-amplify";
import Loading from "../../components/Loading";
import Colors from "../../constants/Colors";
import GradientButton from "../../components/GradientButton";

const SignInScreen = (props) => {
  const [loading, setLoading] = React.useState(false);

  async function onSubmit(values) {
    if (values.email && values.password) {
      setLoading(true);
      try {
        await Auth.signIn(values.email, values.password);
        props.onStateChange("signedIn", {});
        setLoading(false);
      } catch (err) {
        setLoading(false);
        Alert.alert("Error Signing In", err.message, [{ text: "Ok" }]);
      }
    }
  }

  if (props.authState === "signIn") {
    return (
      <View style={styles.screen}>
        {loading === true ? <Loading /> : null}
        <ScrollView>
          <View style={styles.imageContainer}>
            <Image
              source={require("../../../assets/colab-launch-screen.png")}
            />
          </View>

          <Formik
            initialValues={{ email: "", password: "" }}
            onSubmit={(values) => onSubmit(values)}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email("Not a valid email address")
                .required("E-mail is required"),
              password: yup.string().min(8).required("A password is required"),
            })}
          >
            {({
              values,
              handleChange,
              errors,
              handleSubmit,
              touched,
              setFieldTouched,
              dirty,
              isValid,
            }) => (
              <>
                <View style={styles.inputContainer}>
                  <View style={styles.textInput}>
                    <TextInput
                      value={values.email}
                      onChangeText={handleChange("email")}
                      label="E-mail"
                      onBlur={() => setFieldTouched("email")}
                      autoCompleteType="email"
                      autoCapitalize="none"
                      mode="outlined"
                      theme={{ colors: { primary: Colors.primaryColor } }}
                    />
                  </View>
                  {touched.email && errors.email && (
                    <Text style={styles.errorText}>{errors.email}</Text>
                  )}

                  <View style={styles.textInput}>
                    <TextInput
                      value={values.password}
                      onChangeText={handleChange("password")}
                      label="Password"
                      secureTextEntry={true}
                      onBlur={() => setFieldTouched("password")}
                      mode="outlined"
                      theme={{ colors: { primary: Colors.primaryColor } }}
                    />
                  </View>
                  {touched.password && errors.password && (
                    <Text style={styles.errorText}>{errors.password}</Text>
                  )}
                </View>

                <View style={styles.buttonContainer}>
                  <GradientButton
                    style={styles.button}
                    label="Sign In"
                    disabled={!(isValid && dirty)}
                    gradientBegin={Colors.primaryColor}
                    gradientEnd={Colors.secondaryColor}
                    disabledGradientBegin={Colors.primaryColorDisabled}
                    disabledGradientEnd={Colors.secondaryColorDisabled}
                    gradientDirection="vertical"
                    impact
                    impactStyle="Light"
                    onPressAction={handleSubmit}
                  />

                  <Text
                    onPress={() => props.onStateChange("forgotPassword")}
                    style={{
                      fontFamily: "raleway-medium",
                      fontSize: 16,
                      color: Colors.secondaryColor,
                    }}
                  >
                    Forgotten password?
                  </Text>

                  <View style={{ margin: "2%" }} />

                  <Text style={styles.bottomText}>
                    Not a Colaber yet?{" "}
                    <Text
                      onPress={() => props.onStateChange("signUp", {})}
                      style={{ color: Colors.primaryColor }}
                    >
                      Create an account.
                    </Text>
                  </Text>
                </View>
              </>
            )}
          </Formik>
        </ScrollView>
      </View>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 100,
  },
  inputContainer: {
    marginVertical: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    marginHorizontal: "5%",
    padding: 5,
  },
  textInput: {
    marginTop: 10,
  },
  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    marginVertical: 8,
    marginBottom: 20,
  },
  errorText: {
    color: Colors.errorText,
  },
  bottomText: {
    fontFamily: "raleway-medium",
    fontSize: 16,
  },
});

export default SignInScreen;
