import React from "react";
import { View, StyleSheet, Text, Image, ScrollView, Alert } from "react-native";
import { TextInput } from "react-native-paper";
import { Formik } from "formik";
import { Auth } from "aws-amplify";
import * as yup from "yup";

import GradientButton from "../../components/GradientButton";
import Colors from "../../constants/Colors";

const SignUpScreen = (props) => {
  const uowDomain = "uowmail.edu.au";
  const domainErrorMessage =
    "CoLAB currently only supports UOW students. Please use a valid UOW email address.";

  yup.addMethod(yup.string, "domain", function (message = domainErrorMessage) {
    return this.test({
      message,
      test: (value) =>
        value === null ||
        value === "" ||
        value === undefined ||
        value.split("@").pop() === uowDomain,
    });
  });

  async function onSubmit(values) {
    try {
      await Auth.signUp({
        username: values.email,
        password: values.password,
        attributes: {
          name: values.name,
        },
      });

      props.onStateChange("confirmSignUp", {});
    } catch (err) {
      Alert.alert("Error Signing Up", err.message, [{ text: "Ok" }]);
    }
  }

  if (props.authState === "signUp") {
    return (
      <ScrollView>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../../assets/colab-launch-screen.png")}
            style={styles.image}
          />
        </View>

        <View style={styles.inputContainer}>
          <Formik
            initialValues={{
              name: "",
              email: "",
              password: "",
              confirmPassword: "",
            }}
            onSubmit={(values) => onSubmit(values)}
            validationSchema={yup.object().shape({
              name: yup.string().min(2),
              email: yup
                .string()
                .email("Not a valid email address")
                .required("E-mail is required")
                .domain(),
              password: yup
                .string()
                .min(8, "Password must be at least 8 characters")
                .required("A password is required"),
              confirmPassword: yup
                .string()
                .oneOf([yup.ref("password"), null], "Passwords must match"),
            })}
          >
            {({
              values,
              handleChange,
              errors,
              handleSubmit,
              touched,
              setFieldTouched,
              isValid,
              dirty,
            }) => (
              <>
                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.name}
                    onChangeText={handleChange("name")}
                    label="Full Name"
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                </View>

                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.email}
                    onChangeText={handleChange("email")}
                    label="E-mail"
                    onBlur={() => setFieldTouched("email")}
                    autoCompleteType="email"
                    autoCapitalize="none"
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                  {touched.email && errors.email && (
                    <Text style={styles.errorText}>{errors.email}</Text>
                  )}
                </View>

                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.password}
                    onChangeText={handleChange("password")}
                    label="Password"
                    secureTextEntry={true}
                    onBlur={() => setFieldTouched("password")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                  {touched.password && errors.password && (
                    <Text style={styles.errorText}>{errors.password}</Text>
                  )}
                </View>

                <View style={styles.textInput}>
                  <TextInput
                    mode="outlined"
                    value={values.confirmPassword}
                    onChangeText={handleChange("confirmPassword")}
                    label="Confirm Password"
                    secureTextEntry={true}
                    onBlur={() => setFieldTouched("confirmPassword")}
                    theme={{ colors: { primary: Colors.primaryColor } }}
                  />
                  {touched.confirmPassword && errors.confirmPassword && (
                    <Text style={styles.errorText}>
                      {errors.confirmPassword}
                    </Text>
                  )}
                </View>

                <View style={styles.buttonContainer}>
                  <GradientButton
                    style={styles.button}
                    label="GET SMARTER"
                    disabled={!(isValid && dirty)}
                    gradientBegin={Colors.primaryColor}
                    gradientEnd={Colors.secondaryColor}
                    disabledGradientBegin={Colors.primaryColorDisabled}
                    disabledGradientEnd={Colors.secondaryColorDisabled}
                    gradientDirection="vertical"
                    impact
                    impactStyle="Light"
                    onPressAction={handleSubmit}
                  />
                </View>
              </>
            )}
          </Formik>

          <View style={styles.textContainer}>
            <Text style={{ fontFamily: "raleway-medium", fontSize: 16 }}>
              Already have an account?{" "}
              <Text
                style={{ color: Colors.secondaryColor }}
                onPress={() => props.onStateChange("signIn", {})}
              >
                Sign in.
              </Text>
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 100,
  },
  inputContainer: {
    marginVertical: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    padding: 5,
    marginHorizontal: "5%",
  },
  textInput: {
    marginTop: 10,
  },
  errorText: {
    color: Colors.errorText,
  },
  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    marginVertical: 30,
    marginBottom: 20,
  },
  textContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default SignUpScreen;
