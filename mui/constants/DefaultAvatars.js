import { Image } from "react-native";

import avatar1 from "../../assets/avatars/1.png";
import avatar2 from "../../assets/avatars/2.png";
import avatar3 from "../../assets/avatars/3.png";
import avatar4 from "../../assets/avatars/4.png";
import avatar5 from "../../assets/avatars/5.png";
import avatar6 from "../../assets/avatars/6.png";
import avatar7 from "../../assets/avatars/7.png";
import avatar8 from "../../assets/avatars/8.png";
import avatar9 from "../../assets/avatars/9.png";
import avatar10 from "../../assets/avatars/10.png";
import avatar11 from "../../assets/avatars/11.png";
import avatar12 from "../../assets/avatars/12.png";
import avatar13 from "../../assets/avatars/13.png";
import avatar14 from "../../assets/avatars/14.png";
import avatar15 from "../../assets/avatars/15.png";
import avatar16 from "../../assets/avatars/16.png";
import avatar17 from "../../assets/avatars/17.png";
import avatar18 from "../../assets/avatars/18.png";
import avatar19 from "../../assets/avatars/19.png";
import avatar20 from "../../assets/avatars/20.png";
import avatar21 from "../../assets/avatars/21.png";
import avatar22 from "../../assets/avatars/22.png";
import avatar23 from "../../assets/avatars/23.png";
import avatar24 from "../../assets/avatars/24.png";
import avatar25 from "../../assets/avatars/25.png";
import avatar26 from "../../assets/avatars/26.png";
import groupAvatar1 from "../../assets/groups-avatars/1.png";
import groupAvatar2 from "../../assets/groups-avatars/2.png";
import groupAvatar3 from "../../assets/groups-avatars/3.png";
import groupAvatar4 from "../../assets/groups-avatars/4.png";
import groupAvatar5 from "../../assets/groups-avatars/5.png";
import groupAvatar6 from "../../assets/groups-avatars/6.png";
import groupAvatar7 from "../../assets/groups-avatars/7.png";
import groupAvatar8 from "../../assets/groups-avatars/8.png";
import groupAvatar9 from "../../assets/groups-avatars/9.png";
import groupAvatar10 from "../../assets/groups-avatars/10.png";
import groupAvatar11 from "../../assets/groups-avatars/11.png";
import groupAvatar12 from "../../assets/groups-avatars/12.png";
import groupAvatar13 from "../../assets/groups-avatars/13.png";
import groupAvatar14 from "../../assets/groups-avatars/14.png";

export const defaultAvatars = [
  {
    id: 0,
    uri: Image.resolveAssetSource(avatar1).uri,
  },
  {
    id: 1,
    uri: Image.resolveAssetSource(avatar2).uri,
  },
  {
    id: 2,
    uri: Image.resolveAssetSource(avatar3).uri,
  },
  {
    id: 3,
    uri: Image.resolveAssetSource(avatar4).uri,
  },
  {
    id: 4,
    uri: Image.resolveAssetSource(avatar5).uri,
  },
  {
    id: 5,
    uri: Image.resolveAssetSource(avatar6).uri,
  },
  {
    id: 6,
    uri: Image.resolveAssetSource(avatar7).uri,
  },
  {
    id: 7,
    uri: Image.resolveAssetSource(avatar8).uri,
  },
  {
    id: 8,
    uri: Image.resolveAssetSource(avatar9).uri,
  },
  {
    id: 9,
    uri: Image.resolveAssetSource(avatar10).uri,
  },
  {
    id: 10,
    uri: Image.resolveAssetSource(avatar11).uri,
  },
  {
    id: 11,
    uri: Image.resolveAssetSource(avatar12).uri,
  },
  {
    id: 12,
    uri: Image.resolveAssetSource(avatar13).uri,
  },
  {
    id: 13,
    uri: Image.resolveAssetSource(avatar14).uri,
  },
  {
    id: 14,
    uri: Image.resolveAssetSource(avatar15).uri,
  },
  {
    id: 15,
    uri: Image.resolveAssetSource(avatar16).uri,
  },
  {
    id: 16,
    uri: Image.resolveAssetSource(avatar17).uri,
  },
  {
    id: 17,
    uri: Image.resolveAssetSource(avatar18).uri,
  },
  {
    id: 18,
    uri: Image.resolveAssetSource(avatar19).uri,
  },
  {
    id: 19,
    uri: Image.resolveAssetSource(avatar20).uri,
  },
  {
    id: 20,
    uri: Image.resolveAssetSource(avatar21).uri,
  },
  {
    id: 21,
    uri: Image.resolveAssetSource(avatar22).uri,
  },
  {
    id: 22,
    uri: Image.resolveAssetSource(avatar23).uri,
  },
  {
    id: 23,
    uri: Image.resolveAssetSource(avatar24).uri,
  },
  {
    id: 24,
    uri: Image.resolveAssetSource(avatar25).uri,
  },
  {
    id: 25,
    uri: Image.resolveAssetSource(avatar26).uri,
  },
];

export const defaultGroupAvatars = [
  {
    id: 0,
    uri: Image.resolveAssetSource(groupAvatar1).uri,
  },
  {
    id: 1,
    uri: Image.resolveAssetSource(groupAvatar2).uri,
  },
  {
    id: 2,
    uri: Image.resolveAssetSource(groupAvatar3).uri,
  },
  {
    id: 3,
    uri: Image.resolveAssetSource(groupAvatar4).uri,
  },
  {
    id: 4,
    uri: Image.resolveAssetSource(groupAvatar5).uri,
  },
  {
    id: 5,
    uri: Image.resolveAssetSource(groupAvatar6).uri,
  },
  {
    id: 6,
    uri: Image.resolveAssetSource(groupAvatar7).uri,
  },
  {
    id: 7,
    uri: Image.resolveAssetSource(groupAvatar8).uri,
  },
  {
    id: 8,
    uri: Image.resolveAssetSource(groupAvatar9).uri,
  },
  {
    id: 9,
    uri: Image.resolveAssetSource(groupAvatar10).uri,
  },
  {
    id: 10,
    uri: Image.resolveAssetSource(groupAvatar11).uri,
  },
  {
    id: 11,
    uri: Image.resolveAssetSource(groupAvatar12).uri,
  },
  {
    id: 12,
    uri: Image.resolveAssetSource(groupAvatar13).uri,
  },
  {
    id: 13,
    uri: Image.resolveAssetSource(groupAvatar14).uri,
  },
];
