import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import globalStyles from "../styles/globalStyles";
import { Card } from "react-native-shadow-cards";
import { useOnboardingContextState } from "../screens/onboarding/OnboardingContext";

const OnboardingIncompleteDashComponent = (props) => {
  const onboardingState = useOnboardingContextState();

  return (
    <View style={styles.dashboardElement}>
      {props.onboardingIncomplete ? (
        <Card>
          <TouchableOpacity
            onPress={() => onboardingState.setIsSubmissionReceived(false)}
          >
            <View style={globalStyles.groupsRendererCard}>
              <View
                style={{
                  flex: 1,
                  padding: "3%",
                  backgroundColor: "hotpink",
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 5,
                  height: 60,
                }}
              >
                <FontAwesome5Icon name="tasks" size={30} color="white" />
              </View>
              <Text
                style={{
                  flex: 6,
                  marginLeft: "3%",
                  fontFamily: "raleway",
                  alignSelf: "center",
                  justifyContent: "center",
                  fontSize: 14,
                }}
              >
                Complete the onboarding process now to get the most out of CoLAB
              </Text>
              <FontAwesome5Icon name="tasks" size={30} color="white" />
            </View>
          </TouchableOpacity>
        </Card>
      ) : (
        <></>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  dashboardElement: {
    flexDirection: "column",
    marginTop: "5%",
    marginHorizontal: "5%",
    flex: 1,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  notification: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "hotpink",
  },
  notificationText: {
    fontWeight: "bold",
  },
  textBlock: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
    marginLeft: "2%",
  },
});

export default OnboardingIncompleteDashComponent;
