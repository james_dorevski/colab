import React from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import Colors from "../constants/Colors";
import { Avatar } from "react-native-elements";
import {
  defaultAvatars,
  defaultGroupAvatars,
} from "../constants/DefaultAvatars";

const AvatarPicker = (props) => {
  const style = StyleSheet.create({
    container: {
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "center",
    },
    innerText: {
      color: Colors.notSelected,
      fontSize: 18,
    },
  });

  let avatarStyle;
  let defaultAvatarsForPicker;
  if (props.type && props.type === "group") {
    defaultAvatarsForPicker = defaultGroupAvatars;
  } else {
    defaultAvatarsForPicker = defaultAvatars;
  }
  const avatars = defaultAvatarsForPicker.map((item, index) => {
    // Check if the Avatar being rendered has been selected.
    item.id === props.value
      ? (avatarStyle = { borderWidth: 4, borderColor: Colors.charcoal })
      : (avatarStyle = { borderWidth: 0 });

    return (
      <View key={index}>
        <TouchableOpacity onPress={() => props.onValueSelect(item)}>
          <View
            style={{
              paddingHorizontal: 4,
              paddingBottom: 8,
              backgroundColor: "white",
            }}
          >
            <Avatar
              rounded
              source={{ uri: item.uri }}
              size={70}
              avatarStyle={avatarStyle}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  });

  return <View style={style.container}>{avatars}</View>;
};

export default AvatarPicker;
