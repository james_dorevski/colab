import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Button } from "react-native-paper";
import globalStyles from "../styles/globalStyles";
import React from "react";
import Colors from "../constants/Colors";
import { MaterialIcons } from "@expo/vector-icons";
import EmptyStateDashComponent from "./EmptyStateDashComponent";

const NotificationsDashComponent = (props) => {
  //TODO set notification icon and background color based on notification type, and define onPress functionality / also define what happens when you press SHOW ALL
  const returnIcon = (notification) => {
    switch (notification.type) {
      case "internal":
        return { type: "notifications", color: "pink" };
      case "schedule":
        return { type: "schedule", color: "gold" };
      case "group":
        return { type: "group", color: "skyblue" };
      default:
        return "notification-important";
    }
  };

  return (
    <View style={styles.dashboardElement}>
      <View style={styles.header}>
        {props.newNotifications ? (
          <Text style={{ fontFamily: "raleway", fontSize: 20 }}>
            Notifications ({props.newNotifications} new)
          </Text>
        ) : (
          <Text style={{ fontFamily: "raleway", fontSize: 20 }}>
            Notifications
          </Text>
        )}
        <Button mode="text">
          <Text style={styles.actionButton}>Show All</Text>
        </Button>
      </View>
      {props.notifications ? (
        <View style={styles.notificationGroup}>
          {props.notifications.map((notification, idx) => {
            const { type, color } = returnIcon(notification);
            if (idx < 3) {
              return (
                <TouchableOpacity
                  style={styles.notification}
                  key={idx}
                  onPress={() => {}}
                >
                  <View
                    style={{
                      padding: "3%",
                      backgroundColor: color,
                    }}
                  >
                    <MaterialIcons name={type} size={26} color="white" />
                  </View>
                  <View style={styles.textBlock}>
                    <Text style={styles.notificationText} numberOfLines={1}>
                      {notification.text}
                    </Text>
                    <Text>{notification.from}</Text>
                  </View>
                </TouchableOpacity>
              );
            }
          })}
        </View>
      ) : (
        <View style={globalStyles.emptyState}>
          <EmptyStateDashComponent
            text="There's nothing for you to do right now."
            image={require("../../assets/emptyStateScreens/relax_30.png")}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  dashboardElement: {
    flexDirection: "column",
    width: "95%",
    marginHorizontal: "5%",
    marginTop: "5%",
    flex: 1,
  },
  header: {
    marginRight: "3%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  actionButton: {
    color: Colors.secondaryColor,
  },
  notificationGroup: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
  },
  notification: {
    marginTop: "1%",
    flexDirection: "row",
  },
  notificationText: {
    fontWeight: "bold",
  },
  textBlock: {
    flex: 1,
    justifyContent: "center",
    flexWrap: "wrap",
    marginLeft: "2%",
  },
});

export default NotificationsDashComponent;
