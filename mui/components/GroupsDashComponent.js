import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import Colors from "../constants/Colors";
import globalStyles from "../styles/globalStyles";
import EmptyStateDashComponent from "./EmptyStateDashComponent";
import { groupsStub } from "../stubs/stubs";
import GroupCard from "./GroupCard";
import { useUserContextState } from "../UserContext";

const GroupsDashComponent = (props) => {
  const user = useUserContextState();

  let defaultGroups = [];
  //stubs
  user.groups != null || user.groups !== undefined
    ? (defaultGroups = user.groups)
    : (defaultGroups = groupsStub);

  return (
    <View style={styles.dashboardElement}>
      <View style={styles.header}>
        <Text style={{ fontFamily: "raleway", fontSize: 20 }}>
          Your study groups
        </Text>
        <Button mode="text" onPress={props.navigateTo}>
          <Text style={styles.actionButton}>Show All</Text>
        </Button>
      </View>
      {defaultGroups.length > 0 ? (
        <View style={styles.containerStyle}>
          <FlatList
            data={defaultGroups}
            renderItem={({ item, index }) => {
              if (index < 3) {
                return (
                  <GroupCard
                    group={item.group}
                    onPress={() => props.showGroups(item.group)}
                  />
                );
              }
            }}
            keyExtractor={(item) => item.id.toString()}
            ItemSeparatorComponent={() => (
              <View style={{ marginBottom: "3%" }} />
            )}
          />
        </View>
      ) : (
        //display empty state
        <View style={globalStyles.emptyState}>
          <EmptyStateDashComponent
            text="Join a group to see it here."
            image={require("../../assets/emptyStateScreens/groups_30.png")}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  dashboardElement: {
    flexDirection: "column",
    width: "95%",
    marginHorizontal: "5%",
    flex: 1,
  },
  header: {
    marginRight: "3%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  containerStyle: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  text: {
    fontSize: 12,
    textAlign: "center",
  },
  actionButton: {
    color: Colors.secondaryColor,
  },
});

export default GroupsDashComponent;
