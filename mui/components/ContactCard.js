import React from "react";
import { View, Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import { Card } from "react-native-paper";
import { Avatar } from "react-native-elements";
import { defaultAvatars } from "../constants/DefaultAvatars";

const ContactCard = (props) => {
  return (
    <Card style={styles.contactsRenderCard} onPress={props.onPress}>
      <View style={{ flexDirection: "row" }}>
        <Avatar
          source={{ uri: defaultAvatars[props.item.avatar].uri }}
          size={50}
          containerStyle={{ marginHorizontal: "2%" }}
        />
        <View style={styles.userInfoContainer}>
          <Text style={styles.userName}>{props.item.name}</Text>
          {props.item.university !== undefined && (
            <Text style={styles.campusInfo}>
              {props.item.university} | {props.item.campus} |{" "}
              {props.item.subject}
            </Text>
          )}
        </View>
      </View>
    </Card>
  );
};

ContactCard.propTypes = {
  item: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  contactsRenderCard: {
    flexDirection: "row",
    height: 65,
    marginHorizontal: "5%",
    alignItems: "center",
  },
  userInfoContainer: {
    justifyContent: "center",
  },
  userName: {
    fontFamily: "raleway-bold",
    fontSize: 15,
  },
  campusInfo: {
    fontFamily: "raleway",
  },
});

export default ContactCard;
