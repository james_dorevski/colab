import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
// import * as Haptics from 'expo-haptics';
//disabled for now til we figure out the haptics error

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 8
    },
    gradient: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    },
    text: {
        fontSize: 24,
        fontWeight: "bold",
        color: "white"
    }
});

class GradientButton extends React.PureComponent {
    render() {
        const {
            children,
            style,
            label,
            textStyle,
            gradientBegin,
            gradientEnd,
            disabledGradientBegin,
            disabledGradientEnd,
            gradientDirection,
            height,
            width,
            radius,
            impact,
            impactStyle,
            onPressAction,
            disabled,
        } = this.props;

        const disabledColor = [disabledGradientBegin || "#D3D3D3", disabledGradientEnd || "#696969"];

        const horizontalGradient = {
            start: { x: 0, y: 0.5 },
            end: { x: 1, y: 0.5 }
        };

        const verticalGradient = {
            start: { x: 0, y: 0 },
            end: { x: 0, y: 1 }
        };

        const diagonalGradient = {
            start: { x: 0, y: 0 },
            end: { x: 1, y: 1 }
        };

        return (
            <TouchableOpacity
                style={[styles.button, { height, width }, style]}
                onPress={disabled ? null : () => {
                    // if (Platform.OS === "ios" && impact === true) {
                    //     Haptics.impactAsync(Haptics.ImpactFeedbackStyle[impactStyle]);
                    // }
                    if (onPressAction) {
                        return onPressAction();
                    }
                }}
            >
                <LinearGradient
                    style={[styles.gradient, { borderRadius: radius }]}
                    colors={
                        disabled ? disabledColor : [gradientBegin, gradientEnd]
                    }
                    start={
                        gradientDirection === "vertical"
                            ? verticalGradient.start
                            : gradientDirection === "diagonal"
                            ? diagonalGradient.start
                            : horizontalGradient.start
                    }
                    end={
                        gradientDirection === "vertical"
                            ? verticalGradient.end
                            : gradientDirection === "diagonal"
                            ? diagonalGradient.end
                            : horizontalGradient.end
                    }
                >
                    <Text style={[styles.text, textStyle]}>{label ? label : children}</Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}

GradientButton.defaultProps = {
    gradientBegin: "#00d2ff",
    gradientEnd: "#3a47d5",
    gradientDirection: "horizontal",
    height: 60,
    width: 300,
    radius: 12,
    impact: false,
    impactStyle: "Heavy",
    textStyle: { fontSize: 20 },
    disabled: false,
    disabledGradientBegin: "#D3D3D3",
    disabledGradientEnd: "#696969",
};

export default GradientButton;