import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from "react-native";
import { groupsStub } from "../stubs/stubs";
import { defaultGroupAvatars } from "../constants/DefaultAvatars";

const MainGroupFoundComponent = (props) => {
  return (
    <TouchableOpacity style={styles.bestPickContainer} onPress={props.onPress}>
      <ImageBackground
        source={
          props.group.avatar
            ? { uri: defaultGroupAvatars[props.group.avatar].uri }
            : { uri: defaultGroupAvatars[groupsStub[0].avatar].uri }
        }
        style={styles.image}
      >
        <View style={styles.groupNameContainer}>
          <Text style={styles.groupNameText}>{props.group.name}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const MoreGroupsFoundComponent = (props) => {
  const groupWidth = (Dimensions.get("window").width / 2) * 0.9;
  const groupHeight = (Dimensions.get("window").width / 2) * 0.9;
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{ marginLeft: "3%", marginBottom: "3%" }}
    >
      <ImageBackground
        source={
          props.group.avatar
            ? { uri: defaultGroupAvatars[props.group.avatar].uri }
            : { uri: defaultGroupAvatars[groupsStub[0].avatar].uri }
        }
        style={{
          width: groupWidth,
          height: groupHeight,
          justifyContent: "flex-end",
        }}
      >
        <View style={styles.groupNameContainer}>
          <Text style={styles.groupNameText}>{props.group.name}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const FindGroupTabComponent = (props) => {
  /*
   TODO this query needs to be updated to show groups that the student is NOT already a member of
   */
  const returnPotentialGroups = () => {
    return props.potentialGroups.subjects.items.map((subject) => {
      if (subject.subject.code === props.currentSubject.code) {
        if (subject.subject.groups.items.length !== 0) {
          return subject.subject.groups.items.map((groups) => {
            if (groups.group.joinable) {
              return groups.group;
            }
          });
        }
      }
    });
  };
  const potentialGroups = returnPotentialGroups().flat().filter(Boolean);
  return (
    <SafeAreaView style={styles.screen}>
      <FlatList
        ListHeaderComponent={
          <>
            <Text style={styles.bestPickText}>
              Your best pick for {props.currentSubject.code} is:
            </Text>
            {potentialGroups[0] ? (
              <MainGroupFoundComponent
                group={potentialGroups[0]}
                onPress={() => props.showGroups(potentialGroups[0])}
              />
            ) : (
              <MainGroupFoundComponent
                group={groupsStub[0]}
                onPress={() => props.showGroups(groupsStub[0])}
              />
            )}

            <Text style={styles.moreGroupsText}>
              More {props.currentSubject.code} groups
            </Text>
          </>
        }
        data={groupsStub}
        columnWrapperStyle={{ flexWrap: "wrap", flex: 1, marginTop: 5 }}
        horizontal={false}
        numColumns={3}
        keyExtractor={(item) => {
          item.id.toString();
        }}
        renderItem={({ item, index }) => {
          if (index > 0) {
            return (
              <MoreGroupsFoundComponent
                group={item}
                onPress={() => props.showGroups(item)}
              />
            );
          }
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    backgroundColor: "white",
  },
  bestPickText: {
    fontFamily: "raleway-bold",
    fontSize: 20,
    marginLeft: "3%",
    marginVertical: "3%",
  },
  bestPickContainer: {
    marginHorizontal: "3%",
  },
  image: {
    height: 200,
    justifyContent: "flex-end",
  },
  groupNameContainer: {
    backgroundColor: "rgba(0, 0, 0, 0.35)",
  },
  groupNameText: {
    fontFamily: "raleway",
    fontSize: 20,
    color: "white",
    marginLeft: "3%",
    marginTop: "2%",
    marginBottom: "3%",
  },
  moreGroupsText: {
    fontFamily: "raleway",
    fontSize: 20,
    marginLeft: "3%",
    marginTop: "5%",
    marginBottom: "3%",
  },
});

export default FindGroupTabComponent;
