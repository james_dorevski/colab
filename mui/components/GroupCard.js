import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Card } from "react-native-shadow-cards";
import { MaterialIcons } from "@expo/vector-icons";
import { defaultGroupAvatars } from "../constants/DefaultAvatars";

const GroupCard = (props) => {
  const styles = StyleSheet.create({
    groupsRendererCard: {
      height: 65,
      marginTop: "1%",
      flexDirection: "row",
      alignItems: "center",
    },
    groupNameText: {
      fontFamily: "raleway-bold",
      fontSize: 16,
      marginLeft: "5%",
    },
  });

  return (
    <TouchableOpacity onPress={props.onPress}>
      <Card style={styles.groupsRendererCard}>
        <View>
          <Image
            source={{ uri: defaultGroupAvatars[props.group.avatar].uri }}
            style={{ width: 65, height: 65, borderRadius: 5 }}
          />
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Text style={styles.groupNameText}>{props.group.name}</Text>
          <MaterialIcons name={"keyboard-arrow-right"} size={30} />
        </View>
      </Card>
    </TouchableOpacity>
  );
};

export default GroupCard;
