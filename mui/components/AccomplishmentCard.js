import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Card } from "react-native-shadow-cards";
import { MaterialIcons } from "@expo/vector-icons";

const AccomplishmentCard = ({ item }) => {
  const styles = StyleSheet.create({
    accomplishmentCard: {
      width: 110,
      height: 110,
      alignItems: "center",
      marginRight: 20,
    },
  });

  return (
    <Card style={styles.accomplishmentCard}>
      <View
        style={{
          backgroundColor: item.color,
          width: 110,
          height: 80,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <MaterialIcons name={item.icon} size={50} />
      </View>
      <Text style={{ marginTop: "4%", fontFamily: "raleway", fontSize: 16 }}>
        {item.name}
      </Text>
    </Card>
  );
};

export default AccomplishmentCard;
