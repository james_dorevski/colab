import React from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";
import Colors from "../constants/Colors";

const ChipPicker = (props) => {
  const style = StyleSheet.create({
    chipStyle: {
      paddingVertical: 10,
      paddingHorizontal: 10,
      marginVertical: "1%",
      marginHorizontal: "1%",
      borderRadius: 20,
      borderColor: Colors.notSelected,
    },
    container: {
      flexDirection: "row",
      flexWrap: "wrap",
    },
    innerText: {
      color: Colors.notSelected,
      fontSize: 18,
    },
  });

  let selectedColour = Colors.selectedChip;
  if (props.setSelectedColour) {
    selectedColour = props.setSelectedColour;
  }

  let chipStyle = [style.chipStyle];
  const chips = props.items.map((item, index) => {
    // Check if the chip being rendered has been selected.
    item.name === props.value
      ? (chipStyle = [...chipStyle, { backgroundColor: selectedColour }])
      : (chipStyle = [
          ...chipStyle,
          { backgroundColor: Colors.notSelectedLight },
        ]);

    return (
      <TouchableOpacity
        key={index}
        style={chipStyle}
        onPress={() => {
          props.onValueSelect(item);
        }}
      >
        <View>
          <Text style={style.innerText}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  });

  return <View style={style.container}>{chips}</View>;
};

export default ChipPicker;
