import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Modal,
  Text,
  ScrollView,
  Alert,
} from "react-native";
import Colors from "../constants/Colors";
import AvatarPicker from "./AvatarPicker";
import { Button, TextInput } from "react-native-paper";
import { defaultGroupAvatars } from "../constants/DefaultAvatars";
import { Avatar } from "react-native-elements";
import {
  addStudentToGroup,
  addSubjectToGroup,
  createGroup,
} from "../services/GroupService";
import GradientButton from "./GradientButton";
import { Formik } from "formik";

const CreateGroupTabComponent = (props) => {
  const [avatarPickerVisible, setAvatarPickerVisible] = useState(false);
  const [groupCreated, setGroupCreated] = useState(false);
  const [selectedAvatar, setSelectedAvatar] = useState(
    defaultGroupAvatars[13].id
  );

  const onSubmit = (values) => {
    try {
      createGroup(values, selectedAvatar)
        .then((result) => {
          const groupId = result.data.createGroup.id;
          addStudentToGroup(groupId);
          addSubjectToGroup(groupId, props.currentSubject.id);
        })
        .then(() => {
          setGroupCreated(true);
        });
    } catch (err) {
      Alert.alert("We couldn't create your group, try again.", err.message, [
        { text: "OK" },
      ]);
    }
  };

  const GroupCreatedModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={groupCreated}
        onRequestClose={() => setGroupCreated(false)}
      >
        <ScrollView style={styles.modalContainer}>
          <View style={styles.modalView}>
            <Text>Group Created!</Text>
            <Button
              style={styles.modalButton}
              color={Colors.primaryColor}
              onPress={() => {
                setGroupCreated(false);
              }}
            >
              OK
            </Button>
          </View>
        </ScrollView>
      </Modal>
    );
  };
  return (
    <SafeAreaView style={styles.screen}>
      <View style={styles.userDetails}>
        <TouchableOpacity>
          <Avatar
            size={140}
            source={{
              uri: defaultGroupAvatars[selectedAvatar].uri,
            }}
            rounded
          />
          <Avatar.Accessory
            size={26}
            onPress={() => setAvatarPickerVisible(true)}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.inputContainer}>
        <Formik
          initialValues={{
            name: "",
            description: "",
          }}
          onSubmit={(values) => onSubmit(values)}
        >
          {({ values, handleChange, handleSubmit, isValid, dirty }) => (
            <>
              <View style={styles.textInput}>
                <TextInput
                  mode="outlined"
                  value={values.name}
                  onChangeText={handleChange("name")}
                  label="Group Name"
                  theme={{ colors: { primary: Colors.primaryColor } }}
                />
              </View>

              <View style={styles.textInput}>
                <TextInput
                  mode="outlined"
                  value={values.description}
                  onChangeText={handleChange("description")}
                  label="Group Description"
                  autoCapitalize="none"
                  theme={{ colors: { primary: Colors.primaryColor } }}
                />
              </View>

              <View style={styles.buttonContainer}>
                <GradientButton
                  style={styles.button}
                  label="Create Group"
                  disabled={!(isValid && dirty)}
                  gradientBegin={Colors.primaryColor}
                  gradientEnd={Colors.secondaryColor}
                  disabledGradientBegin={Colors.primaryColorDisabled}
                  disabledGradientEnd={Colors.secondaryColorDisabled}
                  gradientDirection="vertical"
                  impact
                  impactStyle="Light"
                  onPressAction={handleSubmit}
                />
              </View>
            </>
          )}
        </Formik>
      </View>

      {groupCreated && <GroupCreatedModal />}

      <View style={styles.modalContainer}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={avatarPickerVisible}
          onRequestClose={() => setAvatarPickerVisible(false)}
        >
          <ScrollView style={styles.modalContainer}>
            <View style={styles.modalView}>
              <AvatarPicker
                type="group"
                value={selectedAvatar}
                onValueSelect={(selectedAvatar) => {
                  setSelectedAvatar(selectedAvatar.id);
                }}
              />
              <Button
                style={styles.modalButton}
                color={Colors.primaryColor}
                onPress={() => setAvatarPickerVisible(false)}
              >
                SET
              </Button>
            </View>
          </ScrollView>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white",
  },
  userDetails: {
    marginVertical: "5%",
    alignItems: "center",
  },
  buttonText: {
    fontSize: 10,
    fontFamily: "raleway-bold",
  },
  profileSection: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: "5%",
  },
  buttonContainer: {
    marginTop: "5%",
  },
  inputContainer: {
    marginVertical: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    padding: 5,
    marginHorizontal: "5%",
  },
  modalContainer: {
    flex: 1,
    alignSelf: "center",
    width: "98%",
    marginVertical: "10%",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButton: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
});

export default CreateGroupTabComponent;
