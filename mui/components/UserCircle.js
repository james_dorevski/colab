import React from "react";
import { Text, TouchableOpacity } from "react-native";
import { Avatar } from "react-native-elements";
import { defaultAvatars } from "../constants/DefaultAvatars";

const UserCircle = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        width: "33%",
        alignItems: "center",
        marginVertical: "2%",
      }}
    >
      <Avatar
        source={{ uri: defaultAvatars[props.user.avatar].uri }}
        size={80}
      />
      <Text numberOfLines={1} style={{ textAlign: "center" }}>
        {props.user.name}
      </Text>
    </TouchableOpacity>
  );
};

export default UserCircle;
