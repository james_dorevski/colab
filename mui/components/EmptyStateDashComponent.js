import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  Dimensions,
} from "react-native";
import React from "react";
import Colors from "../constants/Colors";

const EmptyStateDashComponent = (props) => {
  const text = props.text;
  let image = props.image;

  return (
    <View style={{ height: Dimensions.get("window").height * 0.25 }}>
      <ImageBackground style={styles.imageStyle} source={image}>
        <Text
          style={{
            textAlign: "center",
            textAlignVertical: "center",
            fontSize: 18,
          }}
        >
          {text}
        </Text>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  dashboardElement: {
    flexDirection: "column",
    marginTop: "5%",
    marginHorizontal: "5%",
    flex: 1,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  actionButton: {
    color: Colors.secondaryColor,
  },
  notificationGroup: {
    flexDirection: "column",
    flexWrap: "wrap",
  },
  notification: {
    marginTop: "1%",
    flexDirection: "row",
  },
  notificationText: {
    fontWeight: "bold",
  },
  textBlock: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "wrap",
    marginLeft: "2%",
  },
  imageStyle: {
    flexGrow: 1,
    opacity: 0.45,
    height: "100%",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default EmptyStateDashComponent;
