import React from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { Button } from "react-native-paper";
import Colors from "../constants/Colors";
import EmptyStateDashComponent from "./EmptyStateDashComponent";
import globalStyles from "../styles/globalStyles";
import {
  getUniqueListBy,
  returnCommunity,
  returnUserSubjects,
} from "../services/CommunityService";
import { useUserContextState } from "../UserContext";
import UserCircle from "../components/UserCircle";

const StudyPartnersDashComponent = (props) => {
  const user = useUserContextState();

  const [community, setCommunity] = React.useState([]);

  React.useEffect(() => {
    returnUserSubjects()
      .then((subjects) => {
        const community = returnCommunity(subjects, user);
        const unique = getUniqueListBy(community.flat(), "id");
        setCommunity(unique);
      })
      .then();
  }, []);

  return (
    <View style={styles.dashboardElement}>
      <View style={styles.header}>
        <Text style={{ fontFamily: "raleway", fontSize: 20 }}>CoLAB Users</Text>
        <Button mode="text" onPress={props.navigateTo}>
          <Text style={styles.actionButton}>Search</Text>
        </Button>
      </View>
      <Text style={{ fontFamily: "raleway", fontSize: 12, marginBottom: "2%" }}>
        Showing users in your community
      </Text>
      {community ? (
        <View style={styles.containerStyle}>
          <FlatList
            data={community}
            keyExtractor={(item) => item.id.toString()}
            numColumns={3}
            renderItem={({ item, index }) => {
              if (index < 9) {
                return (
                  <UserCircle
                    user={item}
                    onPress={() => props.showUsers(item)}
                  />
                );
              }
            }}
          />
        </View>
      ) : (
        //display empty state
        <View style={globalStyles.emptyState}>
          <EmptyStateDashComponent
            text="When you connect with other Colabers you will see them here."
            image={require("../../assets/emptyStateScreens/partners_30.png")}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  dashboardElement: {
    flexDirection: "column",
    marginHorizontal: "5%",
    flex: 1,
    marginBottom: "5%",
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  actionButton: {
    color: Colors.secondaryColor,
  },
  containerStyle: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
});

export default StudyPartnersDashComponent;
