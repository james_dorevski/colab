import React from "react";
import { Text } from "react-native";
import { Card } from "react-native-paper";
import Colors from "../../mui/constants/Colors";

const ScheduleCard = (props) => {
  return (
    <Card
      style={{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginVertical: "1%",
        marginHorizontal: "1%",
        backgroundColor: Colors.fafafa,
      }}
    >
      <Card.Content>
        <Text
          style={{
            fontFamily: "raleway",
            fontSize: 18,
            color: props.group.color,
          }}
        >
          {props.group.description}
        </Text>
        <Text>{props.group.day}</Text>
        <Text>
          {props.group.startTime} - {props.group.endTime}
        </Text>
      </Card.Content>
    </Card>
  );
};

export default ScheduleCard;
