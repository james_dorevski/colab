export const groupsStub = [
  {
    avatar: 1,
    description: "Work on Database labs together",
    id: "1",
    name: "CSCI235 Lab Study",
    students: {
      items: [
        {
          student: {
            id: 234,
            avatar: 1,
            name: "Joe Smith",
          },
        },
        {
          student: {
            id: 343,
            avatar: 3,
            name: "Jane Taylor",
          },
        },
      ],
    },
  },
  {
    avatar: 4,
    description: "CSIT321 Kings",
    id: "4",
    name: "CSIT321 Kings",
    students: {
      items: [
        {
          student: {
            id: 304,
            avatar: 20,
            name: "Bob Ross",
          },
        },
      ],
    },
  },
  {
    avatar: 5,
    description: "Study Up ",
    id: "5",
    name: "Studying STAT121",
    students: {
      items: [
        {
          student: {
            id: 1938,
            avatar: 15,
            name: "Mary Sue",
          },
        },
        {
          student: {
            id: 321,
            avatar: 13,
            name: "Fatima Kamal",
          },
        },
        {
          student: {
            id: 123,
            avatar: 1,
            name: "Bea Horseman",
          },
        },
      ],
    },
  },
  {
    avatar: 6,
    description: "Lawyers unite ",
    id: "6",
    name: "LAW284 Study",
    students: {
      items: [
        {
          student: {
            id: 9894,
            avatar: 12,
            name: "Lee Cheng",
          },
        },
      ],
    },
  },
];

export const userStub = {
  name: "Diane Nguyen",
  avatar: require("../../assets/avatars/1.png"),
  wam: "Credit",
  preferredLanguage: "English",
  campus: {
    name: "Wollongong",
  },
  subjects: [{ name: "ISIT315 Semantic Web" }],
  university: {
    name: "University of Wollongong",
  },
  groups: {
    items: [groupsStub],
  },
};

export const friendsStub = [
  {
    id: 1,
    name: "Apu Nahasapeemapetillon",
    avatar: require("../../assets/avatars/1.png"),
  },
  {
    id: 2,
    name: "Barney Gumble",
    avatar: require("../../assets/avatars/13.png"),
  },
  {
    id: 3,
    name: "Mod Flanders",
    avatar: require("../../assets/avatars/10.png"),
  },
  {
    id: 4,
    name: "Lisa Simpson",
    avatar: require("../../assets/avatars/12.png"),
  },
  { id: 5, name: "Cookie Kwan", avatar: require("../../assets/avatars/5.png") },
  {
    id: 6,
    name: "Marge Simpson",
    avatar: require("../../assets/avatars/2.png"),
  },
  { id: 7, name: "Mr Snrub", avatar: require("../../assets/avatars/7.png") },
  {
    id: 8,
    name: "Homer Jay Simpson",
    avatar: require("../../assets/avatars/8.png"),
  },
];
export const directMessageConversationsStub = [
  {
    id: 1,
    name: "GIRLS",
    messages: [
      {
        content: "A message from Bobo the Clown",
        createdAt: "2020-10-25T03:08:53.447Z",
        author: {
          id: "boboID",
          name: "Bobo the Clown",
          avatar: require("../../assets/avatars/26.png"),
        },
      },
      {
        content: "A message from Homer Simpson",
        createdAt: "2020-10-25T03:20:24.888Z",
        author: {
          id: "homerId",
          name: "Homer Simpson",
          avatar: require("../../assets/avatars/20.png"),
        },
      },
      {
        content: "A later message from Bobo the Clown",
        createdAt: "2020-10-26T03:08:53.447Z",
        author: {
          id: "boboId",
          name: "Bobo the Clown",
          avatar: require("../../assets/avatars/26.png"),
        },
      },
    ],
  },
  {
    id: 2,
    name: "Marge Simpson",
    messages: [
      {
        content: "A message from Marge Simpson",
        createdAt: "2020-10-25T03:08:53.447Z",
        author: {
          id: "margeId",
          name: "Marge Simpson",
          avatar: require("../../assets/avatars/25.png"),
        },
      },
      {
        content: "A message from Homer Simpson",
        createdAt: "2020-10-25T03:20:24.888Z",
        author: {
          id: "homerId",
          name: "Homer Simpson",
          avatar: require("../../assets/avatars/20.png"),
        },
      },
      {
        content: "A later message from Marge Simpson",
        createdAt: "2020-10-26T03:08:53.447Z",
        author: {
          id: "margeId",
          name: "Marge Simpson",
          avatar: require("../../assets/avatars/25.png"),
        },
      },
    ],
  },
  {
    id: 3,
    name: "Fatima Kamal",
    messages: [
      {
        content: "A message from Fatima Kamal",
        createdAt: "2020-10-25T03:08:53.447Z",
        author: {
          id: "fatimaId",
          name: "Fatima Kamal",
          avatar: require("../../assets/avatars/14.png"),
        },
      },
    ],
  },
];

export const subjectsStubs = [
  {
    name: "100 Level Subjects",
    id: 0,
    children: [
      {
        name: "CSIT111",
        id: 10,
      },
      {
        name: "CSIT121",
        id: 11,
      },
      {
        name: "CSIT127",
        id: 13,
      },
      {
        name: "CSIT128",
        id: 14,
      },
      {
        name: "CSIT151",
        id: 17,
      },
      {
        name: "CSIT131",
        id: 18,
      },
    ],
  },
  {
    name: "200 Level Subjects",
    id: 1,
    children: [
      {
        name: "CSIT226",
        id: 26,
      },
      {
        name: "CSCI235",
        id: 27,
      },
      {
        name: "CSCI251",
        id: 28,
      },
      {
        name: "CSCI203",
        id: 29,
      },
    ],
  },
  {
    name: "300 Level Subjects",
    id: 2,
    children: [
      {
        name: "CSCI334",
        id: 30,
      },
      {
        name: "CSIT314",
        id: 31,
      },
      {
        name: "ISIT315",
        id: 32,
      },
      {
        name: "CSIT321",
        id: 33,
      },
    ],
  },
];

export const scheduleStub = [
  {
    id: 0,
    day: "Tuesday",
    description: "Weekly Meeting",
    color: "coral",
    startTime: "9:00",
    endTime: "10:00",
  },
  {
    id: 1,
    day: "Thursday",
    description: "Discuss Weekly Lab Work",
    color: "coral",
    startTime: "13:00",
    endTime: "13:30",
  },
];

export const groupThreadStub = [
  {
    id: 0,
    name: "meetings",
  },
  {
    id: 1,
    name: "daily-standup",
  },
  {
    id: 2,
    name: "architecture",
  },
];
