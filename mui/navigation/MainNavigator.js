/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import DashboardStack from "./stacks/DashboardStackNavigator";
import GroupsStack from "./stacks/GroupsStackNavigator";
import BeaconStack from "./stacks/BeaconStackNavigator";
import MessagesStack from "./stacks/MessagesStackNavigator";
import { UserProvider } from "../UserContext";

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
  return (
    <UserProvider>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: Colors.primaryColor,
        }}
      >
        <Tab.Screen
          name="CoLAB"
          component={DashboardStack}
          options={{
            tabBarIcon: ({ color }) => (
              <Ionicons name="md-book" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Groups"
          component={GroupsStack}
          options={{
            tabBarIcon: ({ color }) => (
              <MaterialIcons name="people-outline" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Beacon"
          component={BeaconStack}
          options={{
            tabBarIcon: ({ color }) => (
              <Ionicons name="md-wifi" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Messages"
          component={MessagesStack}
          options={{
            tabBarIcon: ({ color }) => (
              <MaterialIcons
                name="chat-bubble-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </UserProvider>
  );
};

export default MainNavigator;
