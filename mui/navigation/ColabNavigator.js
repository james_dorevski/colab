import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import MainNavigator from "./MainNavigator";
import OnboardingForm from "../screens/onboarding/OnboardingForm";
import { useOnboardingContextState } from "../screens/onboarding/OnboardingContext";

const Stack = createStackNavigator();



const ColabNavigator = () => {

  const onboardingState = useOnboardingContextState();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {onboardingState.isSubmissionReceived === true ? (
          <Stack.Screen
            name="MainTabNavigator"
            component={MainNavigator}
            options={{ headerShown: false }}
          />
        ) : (
          <Stack.Screen
            name="Onboarding"
            component={OnboardingForm}
            options={{ headerShown: false }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default ColabNavigator;
