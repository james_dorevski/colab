import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import GroupsMainScreen from "../../screens/colab/groups/GroupsMainScreen";
import GroupProfileScreen from "../../screens/colab/groups/GroupProfileScreen";
import FindGroupScreen from "../../screens/colab/groups/FindGroupScreen";
import CreateGroupScreen from "../../screens/colab/groups/CreateGroupScreen";

const Stack = createStackNavigator();

const GroupsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="GroupsMain"
        component={GroupsMainScreen}
        options={() => ({
          title: "Your Groups",
        })}
      />
      <Stack.Screen
        name="GroupProfile"
        component={GroupProfileScreen}
        options={({ route }) => ({
          title: route.params.group.name,
        })}
      />
      <Stack.Screen
        name="FindGroup"
        component={FindGroupScreen}
        options={{ headerTitle: "Find Groups" }}
      />
      <Stack.Screen
        name="CreateGroup"
        component={CreateGroupScreen}
        options={{ headerTitle: "Create a Group" }}
      />
    </Stack.Navigator>
  );
};

export default GroupsStack;
