import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import BeaconMainScreen from "../../screens/colab/beacon/BeaconMainScreen";

const Stack = createStackNavigator();

const BeaconStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="BeaconMain"
        component={BeaconMainScreen}
        options={() => ({
          headerLeft: null,
          title: "Send Beacon",
        })}
      />
    </Stack.Navigator>
  );
};

export default BeaconStack;
