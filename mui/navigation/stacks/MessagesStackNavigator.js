import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import MessagesMainScreen from "../../screens/colab/messages/MessagesMainScreen";
import GroupChatsScreen from "../../screens/colab/messages/GroupChatsScreen";
import DirectMessageChatScreen from "../../screens/colab/messages/DirectMessageChatScreen";
import { IconButton } from "react-native-paper";
import NewDirectMessage from "../../screens/colab/messages/NewDirectMessage";

const Stack = createStackNavigator();

const MessagesStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Messages"
        component={MessagesMainScreen}
        options={({ navigation }) => ({
          headerRight: () => (
            <IconButton
              icon="plus"
              size={28}
              onPress={() => navigation.navigate("New Message")}
            />
          ),
          headerLeft: null,
        })}
      />
      <Stack.Screen name="New Message" component={NewDirectMessage} />
      <Stack.Screen
        name="Chat"
        component={DirectMessageChatScreen}
        options={({ route }) => ({
          title: route.params.thread.name,
        })}
      />
      <Stack.Screen
        name="GroupChat"
        component={GroupChatsScreen}
        options={({ route }) => ({
          title: route.params.thread.name,
        })}
      />
    </Stack.Navigator>
  );
};

export default MessagesStack;
