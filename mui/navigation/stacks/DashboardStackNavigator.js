import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import DashboardScreen from "../../screens/colab/DashboardScreen";
import SettingsMainScreen from "../../screens/settings/SettingsMainScreen";
import AccountSettingScreen from "../../screens/settings/AccountSettingsScreen";
import ChangePasswordScreen from "../../screens/settings/ChangePasswordScreen";
import UpdateSubjectScreen from "../../screens/settings/UpdateSubjectScreen";
import UserProfileScreen from "../../screens/colab/UserProfileScreen";
import SearchUsersScreen from "../../screens/colab/SearchUsersScreen";

const Stack = createStackNavigator();

const DashboardStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Dashboard"
        component={DashboardScreen}
        options={{
          headerShown: false,
        }}
      />

      <Stack.Screen name="Settings" component={SettingsMainScreen} />
      <Stack.Screen
        name="AccountSettings"
        component={AccountSettingScreen}
        options={{ headerTitle: "Account" }}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePasswordScreen}
        options={{ headerTitle: "Change Password" }}
      />
      <Stack.Screen
        name="UpdateSubjects"
        component={UpdateSubjectScreen}
        options={{ headerTitle: "Update Subjects" }}
      />
      <Stack.Screen
        name="UserProfile"
        component={UserProfileScreen}
        options={{ headerTitle: "" }}
      />
      <Stack.Screen
        name="SearchUsers"
        component={SearchUsersScreen}
        options={{ headerTitle: "Search Community" }}
      />
    </Stack.Navigator>
  );
};

export default DashboardStack;
