# https://docs.amplify.aws/cli/graphql-transformer/directives#connection
# https://docs.amplify.aws/cli/graphql-transformer/directives#auth
enum CourseType {
  UNDERGRADUATE
  GRADUATE
  POSTGRADUATE
}
enum Residency {
  DOMESTIC
  INTERNATIONAL
}
enum Status {
  PENDING
  ACCEPTED
  DECLINED
  BLOCKED
}
# Student objects are created automatically by a Lambda function upon the user
# confirming their account. "id" is set to the "sub" attribute of the user in
# the Cognito User Pool (i.e. their UID).
type Student
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: owner, ownerField: "id", operations: [update] }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID! # from user in Cognito User Pool
  name: String! # from user in Cognito User Pool
  email: String! # from user in Cognito User Pool
  university: University @connection(name: "UniversityStudents")
  campus: Campus @connection
  groups: [GroupStudent] @connection(name: "StudentGroups")
  requests: [JoinGroupRequests] @connection(name: "StudentRequest")
  availabilities: [Availability] @connection(name: "StudentAvailabilities")
  studentNumber: String
  studentType: Residency # DOMESTIC, INTERNATIONAL
  courseType: CourseType # UNDERGRADUATE, GRADUATE, POSTGRADUATE
  avatar: Int
  languageSpoken: [String]
  preferredLanguage: String
  wam: String
  course: String
  subjects: [StudentSubject] @connection(name: "StudentSubjects")
  updatedInformation: String
  conversations: [ConversationStudentJoin]
    @connection(name: "StudentConversationLink")
  messages: [Message] @connection(name: "StudentMessages")
}
type ConversationStudentJoin @model(queries: null) {
  id: ID!
  conversation: Conversation @connection(name: "ConversationLink")
  student: Student @connection(name: "StudentConversationLink")
}
type Conversation @model {
  id: ID!
  members: [ConversationStudentJoin]! @connection(name: "ConversationLink")
  messages: [Message] @connection(name: "ConversationMessages")
  name: String!
}
type Message @model {
  id: ID!
  conversation: Conversation! @connection(name: "ConversationMessages")
  author: Student! @connection(name: "StudentMessages")
  content: String!
}
type JoinGroupRequests @model(queries: null) {
  id: ID!
  group: Group @connection(name: "GroupRequest")
  student: Student @connection(name: "StudentRequest")
}
type Group
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: owner }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID!
  name: String!
  subjects: [GroupSubject] @connection(name: "GroupSubjects")
  schedule: [TimeSlot] @connection(name: "GroupTimeSlots")
  students: [GroupStudent] @connection(name: "GroupStudents")
  joinable: Boolean!
  description: String
  avatar: Int
  requests: [JoinGroupRequests] @connection(name: "GroupRequest")
}
# Join tables are needed for many-to-many relationships
# TODO: Figure out how we're going to manage authorization here, because it is
# quite complicated.
type GroupStudent @model(queries: null) {
  id: ID!
  group: Group @connection(name: "GroupStudents")
  student: Student @connection(name: "StudentGroups")
}
type GroupSubject @model(queries: null) {
  id: ID!
  group: Group @connection(name: "GroupSubjects")
  subject: Subject @connection(name: "SubjectGroups")
}
type StudentSubject @model(queries: null) {
  id: ID!
  student: Student @connection(name: "StudentSubjects")
  subject: Subject @connection(name: "SubjectStudents")
}
# presumption for friend model
#type StudentStudent @model(queries: null) {
#  id: ID!
#  student: Student @connection(name: "StudentStudent")
#  student: Student @connection(name: "StudentStudent")
#}
type University
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID!
  name: String
  abbreviation: String
  emailDomains: [String]
  courses: [String]
  sessionDuration: String
  subjects: [Subject] @connection(name: "UniversitySubjects")
  students: [Student] @connection(name: "UniversityStudents")
  campuses: [Campus] @connection(name: "UniversityCampuses")
}
type Subject
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID!
  code: String
  name: String
  university: University @connection(name: "UniversitySubjects")
  students: [StudentSubject] @connection(name: "SubjectStudents")
  groups: [GroupSubject] @connection(name: "SubjectGroups")
}
type Campus
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID!
  name: String
  university: University @connection(name: "UniversityCampuses")
}
# Group schedule
type TimeSlot
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: owner }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID!
  day: String
  startTime: String
  description: String
  color: String
  endTime: String
  group: Group @connection(name: "GroupTimeSlots")
}
# Student availability
type Availability
  @model
  @auth(
    rules: [
      { allow: groups, groups: ["Admin"] }
      { allow: owner }
      { allow: private, operations: [read] }
    ]
  ) {
  id: ID!
  day: String
  startTime: String
  endTime: String
  student: Student @connection(name: "StudentAvailabilities")
}
