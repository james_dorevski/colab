# colab

## Running on machine

### Installing npm

You must have npm installed on your machine first. For macOS I recommend homebrew:

```$ brew install npm```

### Installing Expo CLI

```npm install -g expo-cli```

### Running on your device

If you do not want to run the project on a simulator you can run it on your phone by downloading the Expo client app.

### Starting the Expo server

Run `npm start` in the project's root directory to start the server. It should open a link in your browser with tools to run the project on a simulator or your phone.

## Using a Simulator

You need to have Android Studio or Xcode installed to run an Android or iOS simulator respectively. Note that Xcode is only available on macOS.
